{
  "$id": "1",
  "$type": "ExpertSystem.Model.ExpertSystemShell, ExpertSystem",
  "Name": null,
  "Path": "C:\\Users\\Vakoto\\source\\repos\\ExpertSystem\\ExpertSystem\\bin\\Debug\\ExpertSystem.es",
  "Variables": {
    "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Variable, ExpertSystem]], System",
    "$values": [
      {
        "$id": "2",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ЯзыкПрограммирования",
        "Type": 1,
        "Domain": {
          "$id": "3",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "ЯзыкПрограммирования",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "4",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "5",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Go",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "6",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Python",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "7",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C++",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "8",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "TypeScript",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "9",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "JavaScript",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "10",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C#",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "11",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Ruby",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "12",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Php",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "13",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "SqlLite",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "14",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "PostgreSQL",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "15",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C# Unity",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "16",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C++ OpenGL",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "17",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "MATLAB",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "18",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Swift",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "19",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Kotlin",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "20",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C# Xamarin",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "21",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Pascal",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "22",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Maple",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "23",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Mathematica",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "24",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "R",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "25",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Python OpenCV",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "26",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C++ OpenCV",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              },
              {
                "$id": "27",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "C++ Unreal Engine",
                "Domain": {
                  "$ref": "3"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "28",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ТехническоеОграничение",
        "Type": 0,
        "Domain": {
          "$id": "29",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "ТехническоеОграничение",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "30",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "I/O",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              },
              {
                "$id": "31",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "CPU",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              },
              {
                "$id": "32",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Память",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              },
              {
                "$id": "33",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "RealTime",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              },
              {
                "$id": "34",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Встраиваемая система",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              },
              {
                "$id": "35",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Нет",
                "Domain": {
                  "$ref": "29"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Есть ли технические ограничения на проект?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "36",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ЭтоСтудент",
        "Type": 0,
        "Domain": {
          "$id": "37",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "Логический",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "38",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Да",
                "Domain": {
                  "$ref": "37"
                },
                "Original": null
              },
              {
                "$id": "39",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Нет",
                "Domain": {
                  "$ref": "37"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Вы студент?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "40",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "СреднийБаллIT",
        "Type": 0,
        "Domain": {
          "$id": "41",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "СреднийБаллIT",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "42",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Больше чем 4,5",
                "Domain": {
                  "$ref": "41"
                },
                "Original": null
              },
              {
                "$id": "43",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "От 3 до 4,5",
                "Domain": {
                  "$ref": "41"
                },
                "Original": null
              },
              {
                "$id": "44",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Меньше 3",
                "Domain": {
                  "$ref": "41"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Ваш средний балл по IT-дисциплинам.",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "45",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "КоличествоЗаконченныхПроектов",
        "Type": 0,
        "Domain": {
          "$id": "46",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "КоличествоЗаконченныхПроектов",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "47",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Больше 5",
                "Domain": {
                  "$ref": "46"
                },
                "Original": null
              },
              {
                "$id": "48",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Меньше или равно 5",
                "Domain": {
                  "$ref": "46"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Количество законченных (не обязательно проданных) вами проектов.",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "49",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "КоличествоПроданныхПроектов",
        "Type": 0,
        "Domain": {
          "$id": "50",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "КоличествоПроданныхПроектов",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "51",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Больше 3",
                "Domain": {
                  "$ref": "50"
                },
                "Original": null
              },
              {
                "$id": "52",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Меньше или равно 3",
                "Domain": {
                  "$ref": "50"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Количество проданных (продаваемых) проектов.",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "53",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "Самооценка",
        "Type": 0,
        "Domain": {
          "$id": "54",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "Самооценка",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "55",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Высокая",
                "Domain": {
                  "$ref": "54"
                },
                "Original": null
              },
              {
                "$id": "56",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Низкая",
                "Domain": {
                  "$ref": "54"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Ваша самооценка.",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "57",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ОценкаКоллег",
        "Type": 0,
        "Domain": {
          "$id": "58",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "ОценкаКоллег",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "59",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Высокая",
                "Domain": {
                  "$ref": "58"
                },
                "Original": null
              },
              {
                "$id": "60",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Низкая",
                "Domain": {
                  "$ref": "58"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Как вас оценивают ваши коллеги?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "61",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "КоличествоВремениНаРеализацию",
        "Type": 0,
        "Domain": {
          "$id": "62",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "КоличествоВремени",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "63",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Меньше недели",
                "Domain": {
                  "$ref": "62"
                },
                "Original": null
              },
              {
                "$id": "64",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Меньше месяца",
                "Domain": {
                  "$ref": "62"
                },
                "Original": null
              },
              {
                "$id": "65",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "1-3 месяца",
                "Domain": {
                  "$ref": "62"
                },
                "Original": null
              },
              {
                "$id": "66",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "3-6 месяцев",
                "Domain": {
                  "$ref": "62"
                },
                "Original": null
              },
              {
                "$id": "67",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Минимум 6 месяцев",
                "Domain": {
                  "$ref": "62"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Количество времени на реализацию проекта?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "68",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ПроблемнаяОбласть",
        "Type": 0,
        "Domain": {
          "$id": "69",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "ПроблемнаяОбласть",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "70",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Frontend",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "71",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Backend",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "72",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "БД",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "73",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Компьютерная графика",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "74",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Машинное обучение",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "75",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Мобильное приложение",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "76",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Настольное приложение",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "77",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Математика и статистика",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "78",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Big Data",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "79",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Обработка изображений и машинное зрение",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              },
              {
                "$id": "80",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Компьютерная игра",
                "Domain": {
                  "$ref": "69"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "ПроблемнаяОбласть.",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "81",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ЛюбитWindows",
        "Type": 0,
        "Domain": {
          "$ref": "37"
        },
        "QuestionText": "Любите ли вы Windows?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "82",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "МобильнаяОС",
        "Type": 0,
        "Domain": {
          "$id": "83",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "МобильнаяОС",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "84",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "iOS",
                "Domain": {
                  "$ref": "83"
                },
                "Original": null
              },
              {
                "$id": "85",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Android",
                "Domain": {
                  "$ref": "83"
                },
                "Original": null
              },
              {
                "$id": "86",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Windows Phone",
                "Domain": {
                  "$ref": "83"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": "Мобильная ОС, под которую вы собираетесь разрабатывать?",
        "TypeDescription": "Запрашиваемая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "87",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "РезультатРаботы",
        "Type": 1,
        "Domain": {
          "$id": "88",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "РезультатРаботы",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "89",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Высокий",
                "Domain": {
                  "$ref": "88"
                },
                "Original": null
              },
              {
                "$id": "90",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Низкий",
                "Domain": {
                  "$ref": "88"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "91",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ОпытРаботы",
        "Type": 1,
        "Domain": {
          "$id": "92",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "ОпытРаботы",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "93",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Большой",
                "Domain": {
                  "$ref": "92"
                },
                "Original": null
              },
              {
                "$id": "94",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Средний",
                "Domain": {
                  "$ref": "92"
                },
                "Original": null
              },
              {
                "$id": "95",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Маленький",
                "Domain": {
                  "$ref": "92"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "96",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "Квалификация",
        "Type": 1,
        "Domain": {
          "$id": "97",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "Квалификация",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "98",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Сильный студент",
                "Domain": {
                  "$ref": "97"
                },
                "Original": null
              },
              {
                "$id": "99",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Средний студент",
                "Domain": {
                  "$ref": "97"
                },
                "Original": null
              },
              {
                "$id": "100",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Слабый студент",
                "Domain": {
                  "$ref": "97"
                },
                "Original": null
              },
              {
                "$id": "101",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Не студент",
                "Domain": {
                  "$ref": "97"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "102",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "Репутация",
        "Type": 1,
        "Domain": {
          "$id": "103",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "Репутация",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "104",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Высокая",
                "Domain": {
                  "$ref": "103"
                },
                "Original": null
              },
              {
                "$id": "105",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Низкая",
                "Domain": {
                  "$ref": "103"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "106",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "ПриемлемаяСложностьЯзыка",
        "Type": 1,
        "Domain": {
          "$id": "107",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "СложностьЯзыка",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "108",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Сложный",
                "Domain": {
                  "$ref": "107"
                },
                "Original": null
              },
              {
                "$id": "109",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Простой",
                "Domain": {
                  "$ref": "107"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      },
      {
        "$id": "110",
        "$type": "ExpertSystem.Model.Variable, ExpertSystem",
        "Name": "УровеньПрограммиста",
        "Type": 1,
        "Domain": {
          "$id": "111",
          "$type": "ExpertSystem.Model.Domain, ExpertSystem",
          "Name": "УровеньПрограммиста",
          "Values": {
            "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.DomainValue, ExpertSystem]], System",
            "$values": [
              {
                "$id": "112",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Высокий",
                "Domain": {
                  "$ref": "111"
                },
                "Original": null
              },
              {
                "$id": "113",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Средний",
                "Domain": {
                  "$ref": "111"
                },
                "Original": null
              },
              {
                "$id": "114",
                "$type": "ExpertSystem.Model.DomainValue, ExpertSystem",
                "Name": "Низкий",
                "Domain": {
                  "$ref": "111"
                },
                "Original": null
              }
            ]
          },
          "ExpertSystemShell": {
            "$ref": "1"
          },
          "Original": null
        },
        "QuestionText": null,
        "TypeDescription": "Выводимая",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "Original": null
      }
    ]
  },
  "Domains": {
    "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Domain, ExpertSystem]], System",
    "$values": [
      {
        "$ref": "3"
      },
      {
        "$ref": "29"
      },
      {
        "$ref": "41"
      },
      {
        "$ref": "46"
      },
      {
        "$ref": "50"
      },
      {
        "$ref": "54"
      },
      {
        "$ref": "58"
      },
      {
        "$ref": "62"
      },
      {
        "$ref": "69"
      },
      {
        "$ref": "37"
      },
      {
        "$ref": "83"
      },
      {
        "$ref": "88"
      },
      {
        "$ref": "92"
      },
      {
        "$ref": "97"
      },
      {
        "$ref": "103"
      },
      {
        "$ref": "107"
      },
      {
        "$ref": "111"
      }
    ]
  },
  "Rules": {
    "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Rule, ExpertSystem]], System",
    "$values": [
      {
        "$id": "115",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило0",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "116",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "30"
              },
              "ToString": "ТехническоеОграничение = I/O",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "117",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "6"
              },
              "ToString": "ЯзыкПрограммирования = Python",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ I/O КРИТИЧНО ДЛЯ ПРОЕКТА, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ PYTHON",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = I/O ТО ЯзыкПрограммирования = Python",
        "Original": null
      },
      {
        "$id": "118",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило1",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "119",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "31"
              },
              "ToString": "ТехническоеОграничение = CPU",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "120",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "5"
              },
              "ToString": "ЯзыкПрограммирования = Go",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ КОЛИЧЕСТВО CPU КРИТИЧНО ДЛЯ ПРОЕКТА, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ GO",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = CPU ТО ЯзыкПрограммирования = Go",
        "Original": null
      },
      {
        "$id": "121",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило2",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "122",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "32"
              },
              "ToString": "ТехническоеОграничение = Память",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "123",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "7"
              },
              "ToString": "ЯзыкПрограммирования = C++",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ДЛЯ ПРОЕКТА КРИТИЧНО КОЛИЧЕСТВО ПАМЯТИ, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ C++",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Память ТО ЯзыкПрограммирования = C++",
        "Original": null
      },
      {
        "$id": "124",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило3",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "125",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "33"
              },
              "ToString": "ТехническоеОграничение = RealTime",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "126",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "4"
              },
              "ToString": "ЯзыкПрограммирования = C",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ВАШ ПРОЕКТ ТРЕБУЕТ RUNTIME ИСПОЛНЕНИЯ, ТО ОДНОЗНАЧНО БЕРЁМ ЯЗЫК ПРОГРАММИРОВАНИЯ C",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = RealTime ТО ЯзыкПрограммирования = C",
        "Original": null
      },
      {
        "$id": "127",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило4",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "128",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "34"
              },
              "ToString": "ТехническоеОграничение = Встраиваемая система",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "129",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "4"
              },
              "ToString": "ЯзыкПрограммирования = C",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ВЫ РАЗРАБАТЫВАЕТЕ ПРОГРАММУ ДЛЯ ВСТРАЕВАЕМЫХ СИСТЕМ, ТО ОДНОЗНАЧНО ЯЗЫК ПРОГРАММИРОВАНИЯ C",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Встраиваемая система ТО ЯзыкПрограммирования = C",
        "Original": null
      },
      {
        "$id": "130",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило5",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "131",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "132",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "36"
              },
              "DomainValue": {
                "$ref": "38"
              },
              "ToString": "ЭтоСтудент = Да",
              "Original": null
            },
            {
              "$id": "133",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "40"
              },
              "DomainValue": {
                "$ref": "42"
              },
              "ToString": "СреднийБаллIT = Больше чем 4,5",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "134",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "98"
              },
              "ToString": "Квалификация = Сильный студент",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ БАЛЛ СТУДЕНТА БОЛЬШЕ 4.5, ТО ЭТО СИЛЬНЫЙ СТУДЕНТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЭтоСтудент = Да И СреднийБаллIT = Больше чем 4,5 ТО Квалификация = Сильный студент",
        "Original": null
      },
      {
        "$id": "135",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило6",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "136",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "137",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "36"
              },
              "DomainValue": {
                "$ref": "38"
              },
              "ToString": "ЭтоСтудент = Да",
              "Original": null
            },
            {
              "$id": "138",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "40"
              },
              "DomainValue": {
                "$ref": "43"
              },
              "ToString": "СреднийБаллIT = От 3 до 4,5",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "139",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "99"
              },
              "ToString": "Квалификация = Средний студент",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ БАЛЛ СТУДЕНТА ОТ 3 ДО 4.5, ТО ЭТО СРЕДНИЙ СТУДЕНТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЭтоСтудент = Да И СреднийБаллIT = От 3 до 4,5 ТО Квалификация = Средний студент",
        "Original": null
      },
      {
        "$id": "140",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило7",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "141",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "142",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "36"
              },
              "DomainValue": {
                "$ref": "38"
              },
              "ToString": "ЭтоСтудент = Да",
              "Original": null
            },
            {
              "$id": "143",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "40"
              },
              "DomainValue": {
                "$ref": "44"
              },
              "ToString": "СреднийБаллIT = Меньше 3",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "144",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "100"
              },
              "ToString": "Квалификация = Слабый студент",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ БАЛЛ СТУДЕНТА МЕНЬШЕ 3, ТО ЭТО СЛАБЫЙ СТУДЕНТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЭтоСтудент = Да И СреднийБаллIT = Меньше 3 ТО Квалификация = Слабый студент",
        "Original": null
      },
      {
        "$id": "145",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило8",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "146",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "147",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "36"
              },
              "DomainValue": {
                "$ref": "39"
              },
              "ToString": "ЭтоСтудент = Нет",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "148",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "101"
              },
              "ToString": "Квалификация = Не студент",
              "Original": null
            }
          ]
        },
        "Reason": "НЕ СТУДЕНТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЭтоСтудент = Нет ТО Квалификация = Не студент",
        "Original": null
      },
      {
        "$id": "149",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило9",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "150",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "45"
              },
              "DomainValue": {
                "$ref": "47"
              },
              "ToString": "КоличествоЗаконченныхПроектов = Больше 5",
              "Original": null
            },
            {
              "$id": "151",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "49"
              },
              "DomainValue": {
                "$ref": "51"
              },
              "ToString": "КоличествоПроданныхПроектов = Больше 3",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "152",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ЗАКОНЧИЛ БОЛЬШЕ ПЯТИ ПРОЕКТОВ И ПРОДАЛ БОЛЬШЕ ТРЕХ ПРОЕКТОВ, ТО РЕЗУЛЬТАТ РАБОТЫ ВЫСОКИЙ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ КоличествоЗаконченныхПроектов = Больше 5 И КоличествоПроданныхПроектов = Больше 3 ТО РезультатРаботы = Высокий",
        "Original": null
      },
      {
        "$id": "153",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило10",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "154",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "45"
              },
              "DomainValue": {
                "$ref": "47"
              },
              "ToString": "КоличествоЗаконченныхПроектов = Больше 5",
              "Original": null
            },
            {
              "$id": "155",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "49"
              },
              "DomainValue": {
                "$ref": "52"
              },
              "ToString": "КоличествоПроданныхПроектов = Меньше или равно 3",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "156",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ЗАКОНЧИЛ БОЛЬШЕ ПЯТИ ПРОЕКТОВ И ПРОДАЛ МЕНЬШЕ ТРЕХ ПРОЕКТОВ, ТО РЕЗУЛЬТАТ РАБОТЫ НИЗКИЙ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ КоличествоЗаконченныхПроектов = Больше 5 И КоличествоПроданныхПроектов = Меньше или равно 3 ТО РезультатРаботы = Низкий",
        "Original": null
      },
      {
        "$id": "157",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило11",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "158",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "45"
              },
              "DomainValue": {
                "$ref": "48"
              },
              "ToString": "КоличествоЗаконченныхПроектов = Меньше или равно 5",
              "Original": null
            },
            {
              "$id": "159",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "49"
              },
              "DomainValue": {
                "$ref": "51"
              },
              "ToString": "КоличествоПроданныхПроектов = Больше 3",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "160",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ЗАКОНЧИЛ МЕНЬШЕ ПЯТИ ПРОЕКТОВ И ПРОДАЛ БОЛЬШЕ ТРЕХ ПРОЕКТОВ, ТО РЕЗУЛЬТАТ РАБОТЫ ВЫСОКИЙ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ КоличествоЗаконченныхПроектов = Меньше или равно 5 И КоличествоПроданныхПроектов = Больше 3 ТО РезультатРаботы = Высокий",
        "Original": null
      },
      {
        "$id": "161",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило12",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "162",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "45"
              },
              "DomainValue": {
                "$ref": "48"
              },
              "ToString": "КоличествоЗаконченныхПроектов = Меньше или равно 5",
              "Original": null
            },
            {
              "$id": "163",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "49"
              },
              "DomainValue": {
                "$ref": "52"
              },
              "ToString": "КоличествоПроданныхПроектов = Меньше или равно 3",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "164",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ЗАКОНЧИЛ МЕНЬШЕ ПЯТИ ПРОЕКТОВ И ПРОДАЛ МЕНЬШЕ ТРЕХ ПРОЕКТОВ, ТО РЕЗУЛЬТАТ РАБОТЫ НИЗКИЙ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ КоличествоЗаконченныхПроектов = Меньше или равно 5 И КоличествоПроданныхПроектов = Меньше или равно 3 ТО РезультатРаботы = Низкий",
        "Original": null
      },
      {
        "$id": "165",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило13",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "166",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "98"
              },
              "ToString": "Квалификация = Сильный студент",
              "Original": null
            },
            {
              "$id": "167",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "168",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СИЛЬНЫЙ СТУДЕНТ И ВЫСОКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО БОЛЬШОЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Сильный студент И РезультатРаботы = Высокий ТО ОпытРаботы = Большой",
        "Original": null
      },
      {
        "$id": "169",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило14",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "170",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "99"
              },
              "ToString": "Квалификация = Средний студент",
              "Original": null
            },
            {
              "$id": "171",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "172",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ СТУДЕНТ И ВЫСОКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО БОЛЬШОЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Средний студент И РезультатРаботы = Высокий ТО ОпытРаботы = Большой",
        "Original": null
      },
      {
        "$id": "173",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило15",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "174",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "100"
              },
              "ToString": "Квалификация = Слабый студент",
              "Original": null
            },
            {
              "$id": "175",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "176",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "94"
              },
              "ToString": "ОпытРаботы = Средний",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СЛАБЫЙ СТУДЕНТ И ВЫСОКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО СРЕДНИЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Слабый студент И РезультатРаботы = Высокий ТО ОпытРаботы = Средний",
        "Original": null
      },
      {
        "$id": "177",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило16",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "178",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "101"
              },
              "ToString": "Квалификация = Не студент",
              "Original": null
            },
            {
              "$id": "179",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "89"
              },
              "ToString": "РезультатРаботы = Высокий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "180",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НЕ СТУДЕНТ И ВЫСОКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО БОЛЬШОЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Не студент И РезультатРаботы = Высокий ТО ОпытРаботы = Большой",
        "Original": null
      },
      {
        "$id": "181",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило17",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "182",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "98"
              },
              "ToString": "Квалификация = Сильный студент",
              "Original": null
            },
            {
              "$id": "183",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "184",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СИЛЬНЫЙ СТУДЕНТ И НИЗКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО БОЛЬШОЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Сильный студент И РезультатРаботы = Низкий ТО ОпытРаботы = Большой",
        "Original": null
      },
      {
        "$id": "185",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило18",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "186",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "99"
              },
              "ToString": "Квалификация = Средний студент",
              "Original": null
            },
            {
              "$id": "187",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "188",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "95"
              },
              "ToString": "ОпытРаботы = Маленький",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ СТУДЕНТ И НИЗКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО МАЛЕНЬКИЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Средний студент И РезультатРаботы = Низкий ТО ОпытРаботы = Маленький",
        "Original": null
      },
      {
        "$id": "189",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило19",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "190",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "100"
              },
              "ToString": "Квалификация = Слабый студент",
              "Original": null
            },
            {
              "$id": "191",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "192",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "95"
              },
              "ToString": "ОпытРаботы = Маленький",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СЛАБЫЙ СТУДЕНТ И НИЗКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО МАЛЕНЬКИЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Слабый студент И РезультатРаботы = Низкий ТО ОпытРаботы = Маленький",
        "Original": null
      },
      {
        "$id": "193",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило20",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "194",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "96"
              },
              "DomainValue": {
                "$ref": "101"
              },
              "ToString": "Квалификация = Не студент",
              "Original": null
            },
            {
              "$id": "195",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "87"
              },
              "DomainValue": {
                "$ref": "90"
              },
              "ToString": "РезультатРаботы = Низкий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "196",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "95"
              },
              "ToString": "ОпытРаботы = Маленький",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НЕ СТУДЕНТ И НИЗКИЙ РЕЗУЛЬТАТ РАБОТЫ, ТО МАЛЕНЬКИЙ ОПЫТ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Квалификация = Не студент И РезультатРаботы = Низкий ТО ОпытРаботы = Маленький",
        "Original": null
      },
      {
        "$id": "197",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило21",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "198",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "53"
              },
              "DomainValue": {
                "$ref": "55"
              },
              "ToString": "Самооценка = Высокая",
              "Original": null
            },
            {
              "$id": "199",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "57"
              },
              "DomainValue": {
                "$ref": "59"
              },
              "ToString": "ОценкаКоллег = Высокая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "200",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "104"
              },
              "ToString": "Репутация = Высокая",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ВЫСОКО СЕБЯ ОЦЕНИВАЕТ И ВЫСОКО ОЦЕНИВАЕТСЯ КОЛЛЕГАМИ, ТО ВЫСОКАЯ РЕПУТАЦИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Самооценка = Высокая И ОценкаКоллег = Высокая ТО Репутация = Высокая",
        "Original": null
      },
      {
        "$id": "201",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило22",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "202",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "53"
              },
              "DomainValue": {
                "$ref": "55"
              },
              "ToString": "Самооценка = Высокая",
              "Original": null
            },
            {
              "$id": "203",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "57"
              },
              "DomainValue": {
                "$ref": "60"
              },
              "ToString": "ОценкаКоллег = Низкая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "204",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "105"
              },
              "ToString": "Репутация = Низкая",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ВЫСОКО СЕБЯ ОЦЕНИВАЕТ И НИЗКО ОЦЕНИВАЕТСЯ КОЛЛЕГАМИ, ТО НИЗКАЯ РЕПУТАЦИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Самооценка = Высокая И ОценкаКоллег = Низкая ТО Репутация = Низкая",
        "Original": null
      },
      {
        "$id": "205",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило23",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "206",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "53"
              },
              "DomainValue": {
                "$ref": "56"
              },
              "ToString": "Самооценка = Низкая",
              "Original": null
            },
            {
              "$id": "207",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "57"
              },
              "DomainValue": {
                "$ref": "59"
              },
              "ToString": "ОценкаКоллег = Высокая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "208",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "104"
              },
              "ToString": "Репутация = Высокая",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКО СЕБЯ ОЦЕНИВАЕТ И ВЫСОКО ОЦЕНИВАЕТСЯ КОЛЛЕГАМИ, ТО ВЫСОКАЯ РЕПУТАЦИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Самооценка = Низкая И ОценкаКоллег = Высокая ТО Репутация = Высокая",
        "Original": null
      },
      {
        "$id": "209",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило24",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "210",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "53"
              },
              "DomainValue": {
                "$ref": "56"
              },
              "ToString": "Самооценка = Низкая",
              "Original": null
            },
            {
              "$id": "211",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "57"
              },
              "DomainValue": {
                "$ref": "60"
              },
              "ToString": "ОценкаКоллег = Низкая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "212",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "105"
              },
              "ToString": "Репутация = Низкая",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКО СЕБЯ ОЦЕНИВАЕТ И НИЗКО ОЦЕНИВАЕТСЯ КОЛЛЕГАМИ, ТО НИЗКАЯ РЕПУТАЦИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ Самооценка = Низкая И ОценкаКоллег = Низкая ТО Репутация = Низкая",
        "Original": null
      },
      {
        "$id": "213",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило25",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "214",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            },
            {
              "$id": "215",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "104"
              },
              "ToString": "Репутация = Высокая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "216",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "112"
              },
              "ToString": "УровеньПрограммиста = Высокий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ БОЛЬШОЙ ОПЫТ И ВЫСОКАЯ РЕПУТАЦИЯ, ТО ВЫСОКИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Большой И Репутация = Высокая ТО УровеньПрограммиста = Высокий",
        "Original": null
      },
      {
        "$id": "217",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило26",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "218",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "93"
              },
              "ToString": "ОпытРаботы = Большой",
              "Original": null
            },
            {
              "$id": "219",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "105"
              },
              "ToString": "Репутация = Низкая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "220",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ БОЛЬШОЙ ОПЫТ И НИЗКАЯ РЕПУТАЦИЯ, ТО СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Большой И Репутация = Низкая ТО УровеньПрограммиста = Средний",
        "Original": null
      },
      {
        "$id": "221",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило27",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "222",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "94"
              },
              "ToString": "ОпытРаботы = Средний",
              "Original": null
            },
            {
              "$id": "223",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "104"
              },
              "ToString": "Репутация = Высокая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "224",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "112"
              },
              "ToString": "УровеньПрограммиста = Высокий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ ОПЫТ И ВЫСОКАЯ РЕПУТАЦИЯ, ТО ВЫСОКИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Средний И Репутация = Высокая ТО УровеньПрограммиста = Высокий",
        "Original": null
      },
      {
        "$id": "225",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило28",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "226",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "94"
              },
              "ToString": "ОпытРаботы = Средний",
              "Original": null
            },
            {
              "$id": "227",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "105"
              },
              "ToString": "Репутация = Низкая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "228",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ ОПЫТ И НИЗКАЯ РЕПУТАЦИЯ, ТО НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Средний И Репутация = Низкая ТО УровеньПрограммиста = Низкий",
        "Original": null
      },
      {
        "$id": "229",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило29",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "230",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "95"
              },
              "ToString": "ОпытРаботы = Маленький",
              "Original": null
            },
            {
              "$id": "231",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "104"
              },
              "ToString": "Репутация = Высокая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "232",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ МАЛЫЙ ОПЫТ И ВЫСОКАЯ РЕПУТАЦИЯ, ТО СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Маленький И Репутация = Высокая ТО УровеньПрограммиста = Средний",
        "Original": null
      },
      {
        "$id": "233",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило30",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "234",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "91"
              },
              "DomainValue": {
                "$ref": "95"
              },
              "ToString": "ОпытРаботы = Маленький",
              "Original": null
            },
            {
              "$id": "235",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "102"
              },
              "DomainValue": {
                "$ref": "105"
              },
              "ToString": "Репутация = Низкая",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "236",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ МАЛЫЙ ОПЫТ И НИЗКАЯ РЕПУТАЦИЯ, ТО НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ОпытРаботы = Маленький И Репутация = Низкая ТО УровеньПрограммиста = Низкий",
        "Original": null
      },
      {
        "$id": "237",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило31",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "238",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "112"
              },
              "ToString": "УровеньПрограммиста = Высокий",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "239",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ ВЫСОКИЙ УРОВЕНЬ ПРОГРАММИСТА, ТО СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Высокий ТО ПриемлемаяСложностьЯзыка = Сложный",
        "Original": null
      },
      {
        "$id": "240",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило32",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "241",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            },
            {
              "$id": "242",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "64"
              },
              "ToString": "КоличествоВремениНаРеализацию = Меньше месяца",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "243",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 1 МЕСЯЦА, ТО ПРОСТОЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Средний И КоличествоВремениНаРеализацию = Меньше месяца ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "244",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило33",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "245",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            },
            {
              "$id": "246",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "63"
              },
              "ToString": "КоличествоВремениНаРеализацию = Меньше недели",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "247",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 1 МЕСЯЦА, ТО ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Средний И КоличествоВремениНаРеализацию = Меньше недели ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "248",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило34",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "249",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            },
            {
              "$id": "250",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "65"
              },
              "ToString": "КоличествоВремениНаРеализацию = 1-3 месяца",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "251",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ БОЛЬШЕ 1 МЕСЯЦА, ТО СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Средний И КоличествоВремениНаРеализацию = 1-3 месяца ТО ПриемлемаяСложностьЯзыка = Сложный",
        "Original": null
      },
      {
        "$id": "252",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило35",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "253",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            },
            {
              "$id": "254",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "66"
              },
              "ToString": "КоличествоВремениНаРеализацию = 3-6 месяцев",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "255",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ БОЛЬШЕ 1 МЕСЯЦА, ТО СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Средний И КоличествоВремениНаРеализацию = 3-6 месяцев ТО ПриемлемаяСложностьЯзыка = Сложный",
        "Original": null
      },
      {
        "$id": "256",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило36",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "257",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "113"
              },
              "ToString": "УровеньПрограммиста = Средний",
              "Original": null
            },
            {
              "$id": "258",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "67"
              },
              "ToString": "КоличествоВремениНаРеализацию = Минимум 6 месяцев",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "259",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СРЕДНИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ БОЛЬШЕ 1 МЕСЯЦА, ТО СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Средний И КоличествоВремениНаРеализацию = Минимум 6 месяцев ТО ПриемлемаяСложностьЯзыка = Сложный",
        "Original": null
      },
      {
        "$id": "260",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило37",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "261",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            },
            {
              "$id": "262",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "63"
              },
              "ToString": "КоличествоВремениНаРеализацию = Меньше недели",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "263",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 6 МЕСЯЦЕВ, ТО ПРОСТОЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Низкий И КоличествоВремениНаРеализацию = Меньше недели ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "264",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило38",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "265",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            },
            {
              "$id": "266",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "64"
              },
              "ToString": "КоличествоВремениНаРеализацию = Меньше месяца",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "267",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 6 МЕСЯЦЕВ, ТО ПРОСТОЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Низкий И КоличествоВремениНаРеализацию = Меньше месяца ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "268",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило39",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "269",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            },
            {
              "$id": "270",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "65"
              },
              "ToString": "КоличествоВремениНаРеализацию = 1-3 месяца",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "271",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 6 МЕСЯЦЕВ, ТО ПРОСТОЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Низкий И КоличествоВремениНаРеализацию = 1-3 месяца ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "272",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило40",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "273",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            },
            {
              "$id": "274",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "66"
              },
              "ToString": "КоличествоВремениНаРеализацию = 3-6 месяцев",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "275",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ МЕНЬШЕ 6 МЕСЯЦЕВ, ТО ПРОСТОЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Низкий И КоличествоВремениНаРеализацию = 3-6 месяцев ТО ПриемлемаяСложностьЯзыка = Простой",
        "Original": null
      },
      {
        "$id": "276",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило41",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "277",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "110"
              },
              "DomainValue": {
                "$ref": "114"
              },
              "ToString": "УровеньПрограммиста = Низкий",
              "Original": null
            },
            {
              "$id": "278",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "61"
              },
              "DomainValue": {
                "$ref": "67"
              },
              "ToString": "КоличествоВремениНаРеализацию = Минимум 6 месяцев",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "279",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ НИЗКИЙ УРОВЕНЬ ПРОГРАММИСТА И ВРЕМЕНИ БОЛЬШЕ 6 МЕСЯЦЕВ, ТО СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ УровеньПрограммиста = Низкий И КоличествоВремениНаРеализацию = Минимум 6 месяцев ТО ПриемлемаяСложностьЯзыка = Сложный",
        "Original": null
      },
      {
        "$id": "280",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило42",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "281",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "282",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "70"
              },
              "ToString": "ПроблемнаяОбласть = Frontend",
              "Original": null
            },
            {
              "$id": "283",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "284",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "8"
              },
              "ToString": "ЯзыкПрограммирования = TypeScript",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА FRONTEND И СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО TYPESCRIPT",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Frontend И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = TypeScript",
        "Original": null
      },
      {
        "$id": "285",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило43",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "286",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "287",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "70"
              },
              "ToString": "ПроблемнаяОбласть = Frontend",
              "Original": null
            },
            {
              "$id": "288",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "289",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "9"
              },
              "ToString": "ЯзыкПрограммирования = JavaScript",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА FRONTEND И ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО JAVASCRIPT",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Frontend И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = JavaScript",
        "Original": null
      },
      {
        "$id": "290",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило44",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "291",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "292",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "71"
              },
              "ToString": "ПроблемнаяОбласть = Backend",
              "Original": null
            },
            {
              "$id": "293",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "81"
              },
              "DomainValue": {
                "$ref": "38"
              },
              "ToString": "ЛюбитWindows = Да",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "294",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "10"
              },
              "ToString": "ЯзыкПрограммирования = C#",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА BACKEND И МЫ ЛЮБИМ WINDOWS, ТО ЯЗЫК C#",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Backend И ЛюбитWindows = Да ТО ЯзыкПрограммирования = C#",
        "Original": null
      },
      {
        "$id": "295",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило45",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "296",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "297",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "81"
              },
              "DomainValue": {
                "$ref": "39"
              },
              "ToString": "ЛюбитWindows = Нет",
              "Original": null
            },
            {
              "$id": "298",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "299",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "11"
              },
              "ToString": "ЯзыкПрограммирования = Ruby",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА BACKEND, МЫ НЕ ЛЮБИМ WINDOWS И НАМ НУЖЕН СЛОЖНЫЙ ЯП, ТО БЕРЁМ RUBY",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЛюбитWindows = Нет И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = Ruby",
        "Original": null
      },
      {
        "$id": "300",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило46",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "301",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "302",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "81"
              },
              "DomainValue": {
                "$ref": "39"
              },
              "ToString": "ЛюбитWindows = Нет",
              "Original": null
            },
            {
              "$id": "303",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "304",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "12"
              },
              "ToString": "ЯзыкПрограммирования = Php",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА BACKEND, МЫ НЕ ЛЮБИМ WINDOWS И НАМ НУЖЕН ЛЕГКИЙ ЯП, ТО БЕРЁМ PHP",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ЛюбитWindows = Нет И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Php",
        "Original": null
      },
      {
        "$id": "305",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило47",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "306",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "307",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "72"
              },
              "ToString": "ПроблемнаяОбласть = БД",
              "Original": null
            },
            {
              "$id": "308",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "309",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "13"
              },
              "ToString": "ЯзыкПрограммирования = SqlLite",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА БАЗЫ ДАННЫХ И НАМ НУЖЕН ПРОСТОЙ ЯП, ТО БЕРЁМ SQLLITE",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = БД И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = SqlLite",
        "Original": null
      },
      {
        "$id": "310",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило48",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "311",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "312",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "72"
              },
              "ToString": "ПроблемнаяОбласть = БД",
              "Original": null
            },
            {
              "$id": "313",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "314",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "14"
              },
              "ToString": "ЯзыкПрограммирования = PostgreSQL",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА БАЗЫ ДАННЫХ И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК, ТО БЕРЁМ POSTGRESQL",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = БД И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = PostgreSQL",
        "Original": null
      },
      {
        "$id": "315",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило49",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "316",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "317",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "73"
              },
              "ToString": "ПроблемнаяОбласть = Компьютерная графика",
              "Original": null
            },
            {
              "$id": "318",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "319",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "15"
              },
              "ToString": "ЯзыкПрограммирования = C# Unity",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА КОМПЬЮТЕРНАЯ ГРАФИКА И НАМ НУЖЕН ПРОСТОЙ ЯЗЫК, ТО C# UNITY",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Компьютерная графика И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = C# Unity",
        "Original": null
      },
      {
        "$id": "320",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило50",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "321",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "322",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "73"
              },
              "ToString": "ПроблемнаяОбласть = Компьютерная графика",
              "Original": null
            },
            {
              "$id": "323",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "324",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "16"
              },
              "ToString": "ЯзыкПрограммирования = C++ OpenGL",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА КОМПЬЮТЕРНАЯ ГРАФИКА И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК, ТО C++ OPENGL",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Компьютерная графика И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = C++ OpenGL",
        "Original": null
      },
      {
        "$id": "325",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило51",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "326",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "327",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "74"
              },
              "ToString": "ПроблемнаяОбласть = Машинное обучение",
              "Original": null
            },
            {
              "$id": "328",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "329",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "17"
              },
              "ToString": "ЯзыкПрограммирования = MATLAB",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА МАШИННОЕ ОБУЧЕНИЕ И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК, ТО MATLAB",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Машинное обучение И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = MATLAB",
        "Original": null
      },
      {
        "$id": "330",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило52",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "331",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "332",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "74"
              },
              "ToString": "ПроблемнаяОбласть = Машинное обучение",
              "Original": null
            },
            {
              "$id": "333",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "334",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "6"
              },
              "ToString": "ЯзыкПрограммирования = Python",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА МАШИННОЕ ОБУЧЕНИЕ И НАМ НУЖЕН ЛЁГКИЙ ЯЗЫК, ТО PYTHON",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Машинное обучение И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Python",
        "Original": null
      },
      {
        "$id": "335",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило53",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "336",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "337",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "75"
              },
              "ToString": "ПроблемнаяОбласть = Мобильное приложение",
              "Original": null
            },
            {
              "$id": "338",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "82"
              },
              "DomainValue": {
                "$ref": "84"
              },
              "ToString": "МобильнаяОС = iOS",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "339",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "18"
              },
              "ToString": "ЯзыкПрограммирования = Swift",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА МОБИЛЬНОЕ ПРИЛОЖЕНИЕ И ОС IOS, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ SWIFT",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Мобильное приложение И МобильнаяОС = iOS ТО ЯзыкПрограммирования = Swift",
        "Original": null
      },
      {
        "$id": "340",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило54",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "341",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "342",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "75"
              },
              "ToString": "ПроблемнаяОбласть = Мобильное приложение",
              "Original": null
            },
            {
              "$id": "343",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "82"
              },
              "DomainValue": {
                "$ref": "85"
              },
              "ToString": "МобильнаяОС = Android",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "344",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "19"
              },
              "ToString": "ЯзыкПрограммирования = Kotlin",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА МОБИЛЬНОЕ ПРИЛОЖЕНИЕ И ОС ANDROID, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ KOTLIN",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Мобильное приложение И МобильнаяОС = Android ТО ЯзыкПрограммирования = Kotlin",
        "Original": null
      },
      {
        "$id": "345",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило55",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "346",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "347",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "75"
              },
              "ToString": "ПроблемнаяОбласть = Мобильное приложение",
              "Original": null
            },
            {
              "$id": "348",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "82"
              },
              "DomainValue": {
                "$ref": "86"
              },
              "ToString": "МобильнаяОС = Windows Phone",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "349",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "20"
              },
              "ToString": "ЯзыкПрограммирования = C# Xamarin",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА МОБИЛЬНОЕ ПРИЛОЖЕНИЕ И ОС WINDOWS PHONE, ТО ЯЗЫК ПРОГРАММИРОВАНИЯ C# XAMARIN",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Мобильное приложение И МобильнаяОС = Windows Phone ТО ЯзыкПрограммирования = C# Xamarin",
        "Original": null
      },
      {
        "$id": "350",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило56",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "351",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "352",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "76"
              },
              "ToString": "ПроблемнаяОбласть = Настольное приложение",
              "Original": null
            },
            {
              "$id": "353",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "354",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "7"
              },
              "ToString": "ЯзыкПрограммирования = C++",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА НАСТОЛЬНОЕ ПРИЛОЖЕНИЕ И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО С++",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Настольное приложение И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = C++",
        "Original": null
      },
      {
        "$id": "355",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило57",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "356",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "357",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "76"
              },
              "ToString": "ПроблемнаяОбласть = Настольное приложение",
              "Original": null
            },
            {
              "$id": "358",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "359",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "21"
              },
              "ToString": "ЯзыкПрограммирования = Pascal",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА НАСТОЛЬНОЕ ПРИЛОЖЕНИЕ И НАМ НУЖЕН ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО PASCAL",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Настольное приложение И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Pascal",
        "Original": null
      },
      {
        "$id": "360",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило58",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "361",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "362",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "77"
              },
              "ToString": "ПроблемнаяОбласть = Математика и статистика",
              "Original": null
            },
            {
              "$id": "363",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "364",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "22"
              },
              "ToString": "ЯзыкПрограммирования = Maple",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  МАТЕМАТИКА И НАМ НУЖЕН ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО MAPLE",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Математика и статистика И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Maple",
        "Original": null
      },
      {
        "$id": "365",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило59",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "366",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "367",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "77"
              },
              "ToString": "ПроблемнаяОбласть = Математика и статистика",
              "Original": null
            },
            {
              "$id": "368",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "369",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "23"
              },
              "ToString": "ЯзыкПрограммирования = Mathematica",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  МАТЕМАТИКА И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО MATHEMATICA",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Математика и статистика И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = Mathematica",
        "Original": null
      },
      {
        "$id": "370",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило60",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "371",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "372",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "78"
              },
              "ToString": "ПроблемнаяОбласть = Big Data",
              "Original": null
            },
            {
              "$id": "373",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "374",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "6"
              },
              "ToString": "ЯзыкПрограммирования = Python",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  BIG DATA И НАМ НУЖЕН ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО PYTHON",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Big Data И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Python",
        "Original": null
      },
      {
        "$id": "375",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило61",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "376",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "377",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "78"
              },
              "ToString": "ПроблемнаяОбласть = Big Data",
              "Original": null
            },
            {
              "$id": "378",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "379",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "24"
              },
              "ToString": "ЯзыкПрограммирования = R",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  BIG DATA И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО R",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Big Data И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = R",
        "Original": null
      },
      {
        "$id": "380",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило62",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "381",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "382",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "79"
              },
              "ToString": "ПроблемнаяОбласть = Обработка изображений и машинное зрение",
              "Original": null
            },
            {
              "$id": "383",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "384",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "25"
              },
              "ToString": "ЯзыкПрограммирования = Python OpenCV",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  ОБРАБОТКА ИЗОБРАЖЕНИЙ И НАМ НУЖЕН ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО PYTHON OPENCV",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Обработка изображений и машинное зрение И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = Python OpenCV",
        "Original": null
      },
      {
        "$id": "385",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило63",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "386",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "387",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "79"
              },
              "ToString": "ПроблемнаяОбласть = Обработка изображений и машинное зрение",
              "Original": null
            },
            {
              "$id": "388",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "389",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "26"
              },
              "ToString": "ЯзыкПрограммирования = C++ OpenCV",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  ОБРАБОТКА ИЗОБРАЖЕНИЙ СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО C++ OPENCV",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Обработка изображений и машинное зрение И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = C++ OpenCV",
        "Original": null
      },
      {
        "$id": "390",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило64",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "391",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "392",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "80"
              },
              "ToString": "ПроблемнаяОбласть = Компьютерная игра",
              "Original": null
            },
            {
              "$id": "393",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "109"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Простой",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "394",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "15"
              },
              "ToString": "ЯзыкПрограммирования = C# Unity",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  КОМПЬЮТЕРНЫЕ ИГРЫ И НАМ НУЖЕН ЛЕГКИЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО C# UNITY",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Компьютерная игра И ПриемлемаяСложностьЯзыка = Простой ТО ЯзыкПрограммирования = C# Unity",
        "Original": null
      },
      {
        "$id": "395",
        "$type": "ExpertSystem.Model.Rule, ExpertSystem",
        "Name": "Правило65",
        "Premise": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "396",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "28"
              },
              "DomainValue": {
                "$ref": "35"
              },
              "ToString": "ТехническоеОграничение = Нет",
              "Original": null
            },
            {
              "$id": "397",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "68"
              },
              "DomainValue": {
                "$ref": "80"
              },
              "ToString": "ПроблемнаяОбласть = Компьютерная игра",
              "Original": null
            },
            {
              "$id": "398",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "106"
              },
              "DomainValue": {
                "$ref": "108"
              },
              "ToString": "ПриемлемаяСложностьЯзыка = Сложный",
              "Original": null
            }
          ]
        },
        "Conclusion": {
          "$type": "System.Collections.ObjectModel.ObservableCollection`1[[ExpertSystem.Model.Fact, ExpertSystem]], System",
          "$values": [
            {
              "$id": "399",
              "$type": "ExpertSystem.Model.Fact, ExpertSystem",
              "Variable": {
                "$ref": "2"
              },
              "DomainValue": {
                "$ref": "27"
              },
              "ToString": "ЯзыкПрограммирования = C++ Unreal Engine",
              "Original": null
            }
          ]
        },
        "Reason": "ЕСЛИ СФЕРА  КОМПЬЮТЕРНЫЕ ИГРЫ И НАМ НУЖЕН СЛОЖНЫЙ ЯЗЫК ПРОГРАММИРОВАНИЯ, ТО C++ UNREAL ENGINE",
        "ExpertSystemShell": {
          "$ref": "1"
        },
        "ToString": "ЕСЛИ ТехническоеОграничение = Нет И ПроблемнаяОбласть = Компьютерная игра И ПриемлемаяСложностьЯзыка = Сложный ТО ЯзыкПрограммирования = C++ Unreal Engine",
        "Original": null
      }
    ]
  },
  "GoalVariable": null,
  "ExTitle": "ExpertSystem_VADOS_FIXED_2.bin"
}
