﻿using System;
using ExpertSystem.Model;
using ExpertSystem.Ui.Dialogs.Consultation;
using Microsoft.Win32;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json;

namespace ExpertSystem
{
    public partial class MainWindow
    {
        private bool hasChanges = false;

        public ExpertSystemShell ExpertSystem { get; set; }

        public MainWindow()
        {
            ExpertSystem = new ExpertSystemShell();
            InitializeComponent();
            OnExpertSystemChange();
        }

        private void OnExpertSystemChange()
        {
            GetBindingExpression(TitleProperty).UpdateTarget();
            ucRules.ExpertSystem = ExpertSystem;
            ucVariables.ExpertSystem = ExpertSystem;
            ucDomains.ExpertSystem = ExpertSystem;
        }

        private void mItOpenConsultation_Click(object sender, RoutedEventArgs e)
        {
            var consultationDialog = new ConsultationDialog(ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            consultationDialog.ShowDialog();
        }

        private static readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            PreserveReferencesHandling = PreserveReferencesHandling.Objects,
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            TypeNameHandling = TypeNameHandling.All,
            Formatting = Formatting.Indented
        };

        private void SaveExpertSystem()
        {
            try
            {
                var json = JsonConvert.SerializeObject(ExpertSystem, jsonSettings);
                using (var sw = new StreamWriter(ExpertSystem.Path))
                {
                    sw.WriteLine(json);
                }
            }
            catch (Exception ex)
            {
                var num = (int) MessageBox.Show(
                    string.Format("Не удалось сохранить экспертную систему: {0}", ex.Message),
                    "Ошибка сохранения", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
        }

        private void RestoreLinks()
        {
            for (var i = 0; i < ExpertSystem.Domains.Count; i++)
            {
                for (var j = 0; j < ExpertSystem.Domains[i].Values.Count; j++)
                {
                    ExpertSystem.Domains[i].Values[j].Domain = ExpertSystem.Domains[i];
                }
            }
        }

        private void OpenExpertSystem()
        {
            var openFileDialog1 = new OpenFileDialog
            {
                Filter = "ES files (*.es)|*.es|All files (*.*)|*.*",
                FilterIndex = 1,
                RestoreDirectory = true,
                Multiselect = false
            };
            var openFileDialog2 = openFileDialog1;
            var dialogResult = openFileDialog2.ShowDialog();
            if ((dialogResult.GetValueOrDefault() ? dialogResult.HasValue ? 1 : 0 : 0) == 0)
            {
                return;
            }

            var fileName = openFileDialog2.FileName;
            try
            {
                var json = File.ReadAllText(fileName);
                ExpertSystem = JsonConvert.DeserializeObject<ExpertSystemShell>(json, jsonSettings);
                RestoreLinks();
                OnExpertSystemChange();
            }
            catch (Exception ex)
            {
                var num = (int) MessageBox.Show(
                    string.Format("Не удалось загрузить экспертную систему из файла: {0}", ex.Message),
                    "Ошибка сохранения", MessageBoxButton.OK, MessageBoxImage.Hand);
            }

            hasChanges = true;
        }

        private void cmdSaveAs_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var saveFileDialog1 = new SaveFileDialog
            {
                RestoreDirectory = true,
                DefaultExt = ".es",
                FileName = "ExpertSystem"
            };
            var saveFileDialog2 = saveFileDialog1;
            var dialogResult = saveFileDialog2.ShowDialog();
            if ((dialogResult.GetValueOrDefault() ? (dialogResult.HasValue ? 1 : 0) : 0) == 0)
            {
                return;
            }

            ExpertSystem.Path = saveFileDialog2.FileName;
            SaveExpertSystem();
        }

        private void cmdSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (File.Exists(ExpertSystem.Path))
            {
                SaveExpertSystem();
            }
            else
            {
                cmdSaveAs_Executed(sender, e);
            }
        }

        private void cmdOpen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!CheckChanges())
            {
                return;
            }

            OpenExpertSystem();
        }

        private void cmdNew_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!CheckChanges())
            {
                return;
            }

            ExpertSystem = new ExpertSystemShell();
            hasChanges = true;
            OnExpertSystemChange();
        }

        private void mItExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void thisWindow_Closing(object sender, CancelEventArgs e)
        {
            if (CheckChanges())
            {
                return;
            }

            e.Cancel = true;
        }

        private bool CheckChanges()
        {
            if (hasChanges)
            {
                switch (MessageBox.Show("Сохранить изменения?", "Подтверждение", MessageBoxButton.YesNoCancel))
                {
                    case MessageBoxResult.Cancel:
                        return false;
                    case MessageBoxResult.Yes:
                        cmdSave_Executed(null, null);
                        break;
                }
            }

            return true;
        }

        private void mItOpenExplanation_Click(object sender, RoutedEventArgs e)
        {
            if (ExpertSystem.InferenceEngine == null)
            {
                var num = (int) MessageBox.Show("Сначала необходимо начать консультацию.");
            }
            else
            {
                var explanationWindow = new ExplanationWindow(ExpertSystem.InferenceEngine.workingMemory) { Owner = this };
                explanationWindow.ShowDialog();
            }
        }
    }
}
