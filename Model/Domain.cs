﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExpertSystem.Model.Exceptions;
using ExpertSystem.Model.Infrastructure;

namespace ExpertSystem.Model
{
    [Serializable]
    public class Domain : ElementBase<Domain>, IElement
    {
        private string name = "";

        private ObservableCollection<DomainValue> values;

        public string Name
        {
            get => this.name;
            set
            {
                this.ExpertSystemShell?.CheckDomainName(value, this.original?.Name ?? this.Name);
                this.name = value;
                this.OnPropertyChanged(nameof(Name));
            }
        }

        public ObservableCollection<DomainValue> Values
        {
            get => this.values;
            private set
            {
                this.values = value;
                foreach (var domainValue in this.values)
                {
                    domainValue.Domain = this;
                }

                this.OnPropertyChanged(nameof(Values));
            }
        }

        public ExpertSystemShell ExpertSystemShell { get; set; }

        private void DefaultInit(ExpertSystemShell expertSystemShell)
        {
            this.ExpertSystemShell = expertSystemShell;
            this.Name = this.ExpertSystemShell != null ? $"Домен{this.ExpertSystemShell.Domains.Count}" : string.Empty;
            this.Values = new ObservableCollection<DomainValue>();
        }

        public Domain(ExpertSystemShell expertSystemShell)
        {
            this.DefaultInit(expertSystemShell);
        }

        private Domain()
        {
            this.DefaultInit(null);
        }

        private Domain(Domain domain, bool linked)
        {
            if (domain == null)
            {
                throw new ArgumentNullException();
            }

            if (linked)
            {
                this.original = domain;
            }

            this.Name = domain.Name;
            this.ExpertSystemShell = domain.ExpertSystemShell;
            this.Values = domain.CopyValues(linked);
        }

        public ObservableCollection<DomainValue> CopyValues(bool linked)
        {
            return new ObservableCollection<DomainValue>(this.Values.Select(e => e.MakeCopy(linked)));
        }

        private bool ValuesAreEqual(string value, DomainValue domainValue, string nameToIgnore)
        {
            if (domainValue.Name.Equals(value, StringComparison.OrdinalIgnoreCase))
            {
                return !domainValue.Name.Equals(nameToIgnore);
            }

            return false;
        }

        public void CheckDomainValue(string value, string nameToIgnore = "")
        {
            if (value.Length == 0)
            {
                throw new EmptyValueException();
            }

            if (this.Values.Any(domainValue => ValuesAreEqual(value, domainValue, nameToIgnore)))
            {
                throw new DomainValueAlreadyExistsException(value);
            }
        }

        public DomainValue AddValue(string value)
        {
            var domainValue = new DomainValue(this)
            {
                Name = value
            };
            if (this.IsCopy())
            {
                var equal = this.Original
                    .Values
                    .FirstOrDefault(v => v.IsEqualTo(domainValue));

                if (equal != null)
                {
                    domainValue = equal.MakeCopy();
                }
            }

            this.Values.Add(domainValue);
            return domainValue;
        }

        public override bool IsEqualTo(Domain other)
        {
            if (this.Name == other.Name && this.Values.Count == other.Values.Count)
            {
                return this.Values
                    .Zip(other.Values, (v1, v2) => v1.IsEqualTo(v2))
                    .All(areEqual => areEqual);
            }

            return false;
        }

        public override Domain MakeCopy(bool linked = true)
        {
            return new Domain(this, linked);
        }

        public override bool IsValid()
        {
            if (this.ExpertSystemShell != null && this.Name.Length > 0 && this.Values.Count > 0)
            {
                return this.Values.All(e => e.IsValid());
            }

            return false;
        }

        protected override void InnerReplaceOriginal()
        {
            this.original.Name = this.Name;
            var list = new List<DomainValue>();
            foreach (var domainValue in this.Values)
            {
                if (domainValue.IsCopy())
                {
                    domainValue.ReplaceOriginal();
                    list.Add(domainValue.Original);
                }
                else
                {
                    domainValue.Domain = this.original;
                    list.Add(domainValue);
                }
            }

            this.original.Values = new ObservableCollection<DomainValue>(list);
        }

        public override bool IsAtDefaultState()
        {
            return this.IsEqualTo(new Domain(this.ExpertSystemShell));
        }

        public bool HasValuesChanges()
        {
            if (!this.IsCopy())
            {
                return false;
            }

            if (this.Values.Count == this.Original.Values.Count)
            {
                return this.Values
                    .Zip(this.Original.Values, (v1, v2) => v1.IsEqualTo(v2))
                    .Any(areEqual => !areEqual);
            }

            return true;
        }
    }
}
