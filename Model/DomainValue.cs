﻿using System;
using ExpertSystem.Model.Infrastructure;

namespace ExpertSystem.Model
{
    [Serializable]
    public class DomainValue : ElementBase<DomainValue>
    {
        private string name;

        public string Name
        {
            get => this.name;
            set
            {
                this.Domain?.CheckDomainValue(value, this.name);
                this.name = value;
                this.OnPropertyChanged(nameof(Name));
            }
        }

        public Domain Domain { get; set; }

        public DomainValue(Domain domain)
        {
            this.Domain = domain;
        }

        private DomainValue()
        {
        }

        private DomainValue(DomainValue value, bool linked)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }

            if (linked)
            {
                this.original = value;
            }

            this.Name = value.Name;
            this.Domain = value.Domain;
        }

        public override bool IsEqualTo(DomainValue other)
        {
            return this.Name == other.Name;
        }

        public override bool IsValid()
        {
            return this.Name.Length > 0;
        }

        protected override void InnerReplaceOriginal()
        {
            this.original.Name = this.Name;
        }

        public override DomainValue MakeCopy(bool linked = true)
        {
            return new DomainValue(this, linked);
        }

        public override bool IsAtDefaultState()
        {
            return this.IsEqualTo(new DomainValue(this.Domain));
        }
    }
}
