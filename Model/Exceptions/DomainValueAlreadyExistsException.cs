﻿using System;

namespace ExpertSystem.Model.Exceptions
{
    [Serializable]
    public class DomainValueAlreadyExistsException : Exception
    {
        public string Value;

        public DomainValueAlreadyExistsException(string value)
        {
            this.Value = value;
        }
    }
}
