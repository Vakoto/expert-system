﻿using System;

namespace ExpertSystem.Model.Exceptions
{
    [Serializable]
    public class EmptyValueException : Exception
    {
    }
}
