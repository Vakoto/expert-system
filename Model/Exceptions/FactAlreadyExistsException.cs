﻿using System;

namespace ExpertSystem.Model.Exceptions
{
    [Serializable]
    public class FactAlreadyExistsException : Exception
    {
        public string Name;

        public FactAlreadyExistsException(string name)
        {
            this.Name = name;
        }
    }
}
