﻿using System;

namespace ExpertSystem.Model.Exceptions
{
    [Serializable]
    public class InferenceException : Exception
    {
    }
}
