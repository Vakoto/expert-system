﻿using System;

namespace ExpertSystem.Model.Exceptions
{
    [Serializable]
    public class NameAlreadyExistsException : Exception
    {
        public string Name;

        public NameAlreadyExistsException(string name)
        {
            this.Name = name;
        }
    }
}
