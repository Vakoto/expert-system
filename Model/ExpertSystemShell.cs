﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExpertSystem.Model.Exceptions;
using ExpertSystem.Model.Infrastructure;
using Newtonsoft.Json;

namespace ExpertSystem.Model
{
    [Serializable]
    public class ExpertSystemShell : Notifier
    {
        private string path = "";

        public string Name { get; set; }

        public string Path
        {
            get => this.path;
            set
            {
                this.path = value;
                this.OnPropertyChanged(nameof(Path));
                this.OnPropertyChanged("ExTitle");
            }
        }

        [JsonIgnore]
        public InferenceEngine InferenceEngine { get; set; }

        public ObservableCollection<Variable> Variables { get; set; }

        public ObservableCollection<Domain> Domains { get; set; }

        public ObservableCollection<Rule> Rules { get; set; }

        public Variable GoalVariable { get; set; }

        public string ExTitle
        {
            get
            {
                if (this.Path.Length == 0)
                {
                    return string.Empty;
                }

                return System.IO.Path.GetFileName(this.Path);
            }
        }

        public ExpertSystemShell()
        {
            this.Variables = new ObservableCollection<Variable>();
            this.Domains = new ObservableCollection<Domain>();
            this.Rules = new ObservableCollection<Rule>();
        }

        private bool InnerRemove<T1, T2>(
            T1 itemToRemove,
            ICollection<T1> collection,
            IEnumerable<T2> dependingItems,
            bool cascade = false)
            where T2 : IElement
        {
            var areDependent = dependingItems.Any();
            if (cascade & areDependent)
            {
                foreach (var item in dependingItems)
                {
                    this.Remove(item, true);
                }
            }

            if (!cascade && areDependent)
            {
                return false;
            }

            collection.Remove(itemToRemove);
            return true;
        }

        public bool Remove(IElement element, bool cascade = false)
        {
            switch (element)
            {
                case Domain domain:
                    return this.Remove(domain, cascade);
                case Variable variable:
                    return this.Remove(variable, cascade);
                case Rule rule:
                    return this.Remove(rule);
                default:
                    throw new ArgumentException(nameof(element));
            }
        }

        public bool Remove(Domain domain, bool cascade = false)
        {
            return this.InnerRemove(domain, this.Domains,
                this.GetVariablesWithDomain(domain), cascade);
        }

        public bool Remove(Variable variable, bool cascade = false)
        {
            return this.InnerRemove(variable, this.Variables, 
                this.GetRulesWithVariable(variable), cascade);
        }

        public bool Remove(Rule rule)
        {
            this.Rules.Remove(rule);
            return true;
        }

        private IEnumerable<Variable> GetVariablesWithDomain(Domain domain)
        {
            return this.Variables.Where(e => e.Domain == domain);
        }

        private IEnumerable<Rule> GetRulesWithVariable(Variable variable)
        {
            return this.Rules
                .Where(rule => rule.Premise.Any(p => p.Variable == variable))
                .Union(this.Rules.Where(e => e.Conclusion.Any(p => p.Variable == variable)));
        }

        private static bool ElementNamesAreEqual<T>(T element, string name, string nameToIgnore)
            where T : IElement
        {
            if (element.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
            {
                return element.Name != nameToIgnore;
            }

            return false;
        }

        private static void CheckUniqueName<T>(IEnumerable<T> collection, string name, string nameToIgnore = "")
            where T : IElement
        {
            if (collection.Any(element => ElementNamesAreEqual(element, name, nameToIgnore)))
            {
                throw new NameAlreadyExistsException(name);
            }
        }

        public void CheckDomainName(string name, string nameToIgnore = "")
        {
            CheckUniqueName(this.Domains, name, nameToIgnore);
        }

        public void CheckVariableName(string name, string nameToIgnore = "")
        {
            CheckUniqueName(this.Variables, name, nameToIgnore);
        }

        public void CheckRuleName(string name, string nameToIgnore = "")
        {
            CheckUniqueName(this.Rules, name, nameToIgnore);
        }
    }
}
