﻿using System;
using System.ComponentModel;
using ExpertSystem.Model.Infrastructure;

namespace ExpertSystem.Model
{
    [Serializable]
    public class Fact : ElementBase<Fact>
    {
        private Variable variable;

        private DomainValue domainValue;

        public Variable Variable
        {
            get => this.variable;
            set
            {
                this.DomainValue = null;
                this.variable = value;
                this.OnPropertyChanged(nameof(Variable));
            }
        }

        public DomainValue DomainValue
        {
            get => this.domainValue;
            set
            {
                this.domainValue = value;
                this.OnPropertyChanged(nameof(DomainValue));
            }
        }

        public Fact()
        {
            this.PropertyChanged += this.OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ToString")
            {
                return;
            }

            this.OnPropertyChanged("ToString");
        }

        private Fact(Fact fact, bool linked)
            : this()
        {
            if (fact == null)
            {
                throw new ArgumentNullException();
            }

            if (linked)
            {
                this.original = fact;
            }

            this.Variable = fact.Variable;
            this.DomainValue = fact.DomainValue;
        }

        public override bool IsAtDefaultState()
        {
            return this.IsEqualTo(new Fact());
        }

        public override bool IsEqualTo(Fact other)
        {
            if (this.Variable == other.Variable)
            {
                return this.DomainValue == other.DomainValue;
            }

            return false;
        }

        public override bool IsValid()
        {
            if (this.Variable != null && this.DomainValue != null)
            {
                return this.DomainValue.Domain == this.Variable.Domain;
            }

            return false;
        }

        public override Fact MakeCopy(bool linked = true)
        {
            return new Fact(this, linked);
        }

        protected override void InnerReplaceOriginal()
        {
            this.original.Variable = this.Variable;
            this.original.DomainValue = this.DomainValue;
        }

        public string ToString
        {
            get { return $"{this.Variable.Name} = {this.DomainValue?.Name}"; }
        }
    }
}
