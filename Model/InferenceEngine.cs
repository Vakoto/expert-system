﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpertSystem.Model.Exceptions;
using Newtonsoft.Json;

namespace ExpertSystem.Model
{
    [Serializable]
    public class InferenceEngine
    {
        [field:NonSerialized]
        [JsonIgnore]
        public WorkingMemory workingMemory;

        public ExpertSystemShell ExpertSystemShell { get; }

        public InferenceEngine(ExpertSystemShell expertSystemShell)
        {
            this.ExpertSystemShell = expertSystemShell;
            this.InitializeMemory();
        }

        private void InitializeMemory()
        {
            this.workingMemory = new WorkingMemory()
            {
                UntriggeredRules = this.ExpertSystemShell.Rules.ToList()
            };
        }

        private DomainValue RequestVariable(Variable variable)
        {
            var e = new VariableRequestEventArgs()
            {
                Variable = variable
            };
            var variableRequested = this.VariableRequested;
            variableRequested?.Invoke(this, e);
            if (e.Value == null)
            {
                throw new ArgumentNullException();
            }

            return e.Value;
        }

        public event EventHandler<VariableRequestEventArgs> VariableRequested;

        public event EventHandler<VariableDeducedEventArgs> VariableDeduced;

        public void DeduceGoalVariable(Variable variable)
        {
            var domainValue = (DomainValue) null;
            var e = new VariableDeducedEventArgs();
            this.workingMemory.GoalVariable = variable;
            try
            {
                domainValue = this.Deduce(variable);
            }
            catch (InferenceException)
            {
            }
            catch (Exception ex)
            {
                e.HasError = true;
                e.Error = ex.ToString();
            }

            e.Value = domainValue;
            var variableDeduced = this.VariableDeduced;
            variableDeduced?.Invoke(this, e);
        }

        private DomainValue Deduce(Variable variable)
        {
            var fact = this.workingMemory
                .TrueFacts
                .FirstOrDefault(x => x.Variable == variable);

            if (fact != null)
            {
                return fact.DomainValue;
            }

            if (variable.Type == VariableType.Requested)
            {
                var domainValue = this.RequestVariable(variable);
                this.workingMemory.TrueFacts.Add(new Fact
                {
                    Variable = variable,
                    DomainValue = domainValue
                });
                return domainValue;
            }

            foreach (var rule in this.workingMemory
                .UntriggeredRules
                .Where(r => r.Conclusion.Any(c => c.Variable == variable))
                .ToList())
            {
                var source = this.TryApplyRule(rule);
                if (source != null)
                {
                    this.workingMemory.TrueFacts.AddRange(source);
                    return source.First(x => x.Variable == variable).DomainValue;
                }
            }

            return null;
        }

        private List<Fact> TryApplyRule(Rule rule)
        {
            this.workingMemory.UntriggeredRules.Remove(rule);
            foreach (var fact in rule.Premise)
            {
                var fact1 = fact;
                var fact2 = this.workingMemory
                    .TrueFacts
                    .FirstOrDefault(x => x.Variable == fact1.Variable);

                var domainValue = fact2 != null ? fact2.DomainValue : this.Deduce(fact1.Variable);
                if (fact1.DomainValue != domainValue)
                {
                    return null;
                }
            }

            foreach (var fact in rule.Conclusion)
            {
                this.workingMemory.InferenceDictionary[fact.Variable] = rule;
            }

            return rule.Conclusion.ToList();
        }
    }
}