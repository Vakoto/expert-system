﻿using System;

namespace ExpertSystem.Model.Infrastructure
{
    [Serializable]
    public abstract class ElementBase<T> : Notifier
    {
        protected T original;

        public T Original => this.original;

        public bool IsCopy()
        {
            return this.original != null;
        }

        public void MakeOriginal()
        {
            this.original = default(T);
        }

        public bool IsEqualToOriginal()
        {
            if (this.original != null)
            {
                return this.IsEqualTo(this.original);
            }

            return true;
        }

        public void ReplaceOriginal()
        {
            if (this.original == null)
            {
                return;
            }

            this.InnerReplaceOriginal();
        }

        public bool HasChanges()
        {
            if (this.IsCopy())
            {
                return !this.IsEqualToOriginal();
            }

            return !this.IsAtDefaultState();
        }

        public abstract T MakeCopy(bool linked = true);

        public abstract bool IsEqualTo(T other);

        public abstract bool IsValid();

        public abstract bool IsAtDefaultState();

        protected abstract void InnerReplaceOriginal();
    }
}
