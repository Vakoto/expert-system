﻿namespace ExpertSystem.Model.Infrastructure
{
    public interface IElement
    {
        string Name { get; set; }
    }
}
