﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ExpertSystem.Model.Infrastructure
{
    [Serializable]
    public abstract class Notifier : INotifyPropertyChanged
    {
        [field:NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            var propertyChanged = this.PropertyChanged;
            propertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
