﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using ExpertSystem.Model.Exceptions;
using ExpertSystem.Model.Infrastructure;

namespace ExpertSystem.Model
{
    [Serializable]
    public class Rule : ElementBase<Rule>, IElement
    {
        private string name;

        private ObservableCollection<Fact> premise;

        private ObservableCollection<Fact> conclusion;

        private string reason;

        public string Name
        {
            get => this.name;
            set
            {
                this.ExpertSystemShell?.CheckRuleName(value, this.original?.Name ?? this.Name);
                this.name = value;
                this.OnPropertyChanged(nameof(Name));
            }
        }

        public ObservableCollection<Fact> Premise
        {
            get => this.premise;
            set
            {
                this.premise = value;
                this.OnPropertyChanged(nameof(Premise));
                this.OnPropertyChanged("ToString");
            }
        }

        public ObservableCollection<Fact> Conclusion
        {
            get => this.conclusion;
            set
            {
                this.conclusion = value;
                this.OnPropertyChanged(nameof(Conclusion));
                this.OnPropertyChanged("ToString");
            }
        }

        public string Reason
        {
            get => this.reason;
            set
            {
                this.reason = value;
                this.OnPropertyChanged(nameof(Reason));
            }
        }

        public ExpertSystemShell ExpertSystemShell { get; set; }

        public void DefaultInit()
        {
            this.Name = this.ExpertSystemShell != null ? $"Правило{this.ExpertSystemShell.Rules.Count}" : "";
            this.Premise = new ObservableCollection<Fact>();
            this.Conclusion = new ObservableCollection<Fact>();
            this.Reason = "";
            this.PropertyChanged += this.Rule_PropertyChanged;
        }

        private void Rule_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ToString")
            {
                return;
            }

            this.OnPropertyChanged("ToString");
        }

        private Rule()
        {
            this.DefaultInit();
        }

        public Rule(ExpertSystemShell expertSystemShell)
        {
            this.ExpertSystemShell = expertSystemShell;
            this.DefaultInit();
        }

        private Rule(Rule rule, bool linked)
        {
            if (rule == null)
            {
                return;
            }

            if (linked)
            {
                this.original = rule;
            }

            this.ExpertSystemShell = rule.ExpertSystemShell;
            this.Name = rule.Name;
            this.Premise = CopyFacts(rule.Premise);
            this.Conclusion = CopyFacts(rule.Conclusion);
            this.Reason = rule.Reason;
        }

        private static ObservableCollection<Fact> CopyFacts(IEnumerable<Fact> factCollection)
        {
            return new ObservableCollection<Fact>(factCollection.Select(e => e.MakeCopy(false)));
        }

        public override Rule MakeCopy(bool linked = true)
        {
            return new Rule(this, linked);
        }

        public override bool IsEqualTo(Rule other)
        {
            if (this.Name == other.Name && 
                this.Reason == other.Reason &&
                this.Premise.Count == other.Premise.Count &&
                this.Premise.Zip(other.Premise, (f1, f2) => f1.IsEqualTo(f2)).All(areEqual => areEqual) &&
                this.Conclusion.Count == other.Conclusion.Count)
            {
                return this.Conclusion
                    .Zip(other.Conclusion, (f1, f2) => f1.IsEqualTo(f2))
                    .All(x => x);
            }

            return false;
        }

        public override bool IsValid()
        {
            if (this.Name.Length > 0 && this.Premise.Count > 0)
            {
                return this.Conclusion.Count > 0;
            }

            return false;
        }

        public override bool IsAtDefaultState()
        {
            return this.IsEqualTo(new Rule(this.ExpertSystemShell));
        }

        protected override void InnerReplaceOriginal()
        {
            this.original.Name = this.Name;
            this.original.Reason = this.Reason;
            this.original.Premise = CopyFacts(this.Premise);
            this.original.Conclusion = CopyFacts(this.Conclusion);
        }

        public void AddPremise(Fact fact)
        {
            if (this.Premise.Any(e => e.Variable == fact.Variable))
            {
                throw new FactAlreadyExistsException(fact.Variable.Name);
            }

            this.Premise.Add(fact);
        }

        public void AddConclusion(Fact fact)
        {
            if (this.Conclusion.Any(e => e.Variable == fact.Variable))
            {
                throw new FactAlreadyExistsException(fact.Variable.Name);
            }

            this.Conclusion.Add(fact);
        }

        public string ToString
        {
            get
            {
                return "ЕСЛИ " +
                       string.Join(" И ", this.Premise.Select(e => e.ToString)) + " ТО " +
                       string.Join(" И ", this.Conclusion.Select(e => e.ToString));
            }
        }
    }
}
