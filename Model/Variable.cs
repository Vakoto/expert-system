﻿using System;
using System.ComponentModel;
using ExpertSystem.Model.Infrastructure;

namespace ExpertSystem.Model
{
    [Serializable]
    public class Variable : ElementBase<Variable>, IElement
    {
        private string name = "";

        private string questionText = "";

        private VariableType type;

        private Domain domain;

        public string Name
        {
            get => name;
            set
            {
                this.ExpertSystemShell?.CheckVariableName(value, this.original?.Name ?? this.Name);
                this.name = value;
                this.OnPropertyChanged(nameof(Name));
                this.OnPropertyChanged("QuestionText");
            }
        }

        public VariableType Type
        {
            get => this.type;
            set
            {
                this.type = value;
                this.OnPropertyChanged(nameof(Type));
                this.OnPropertyChanged("TypeDescription");
                this.OnPropertyChanged("QuestionText");
            }
        }

        public Domain Domain
        {
            get => this.domain;
            set
            {
                this.domain = value;
                this.OnPropertyChanged(nameof(Domain));
            }
        }

        public string QuestionText
        {
            get
            {
                if (this.Type == VariableType.Deducible)
                {
                    return null;
                }

                if (this.questionText.Length == 0 && this.Name.Length != 0)
                {
                    return this.GenerateQuestionFromName();
                }

                return this.questionText;
            }
            set
            {
                this.questionText = value ?? "";
                this.OnPropertyChanged(nameof(QuestionText));
            }
        }

        public string TypeDescription => GetTypeDescription(this.Type);

        public string GetTypeDescription(VariableType variableType)
        {
            return (typeof(VariableType)
                .GetMember(variableType.ToString())[0]
                .GetCustomAttributes(typeof(DescriptionAttribute), false)[0] as DescriptionAttribute)?
                .Description;
        }

        public ExpertSystemShell ExpertSystemShell { get; set; }

        private void DefaultInit(ExpertSystemShell expertSystemShell)
        {
            this.ExpertSystemShell = expertSystemShell;
            if (this.ExpertSystemShell != null)
            {
                this.Name = $"Переменная{this.ExpertSystemShell.Variables.Count}";
            }

            this.Type = VariableType.Deducible;
        }

        public Variable(ExpertSystemShell expertSystemShell)
        {
            this.DefaultInit(expertSystemShell);
        }

        private Variable(Variable variable, bool linked)
        {
            if (variable == null)
            {
                return;
            }

            if (linked)
            {
                this.original = variable;
            }

            this.ExpertSystemShell = this.original.ExpertSystemShell;
            this.Name = variable.Name;
            this.Type = variable.Type;
            this.Domain = variable.Domain;
            this.QuestionText = variable.QuestionText;
        }

        public override bool IsEqualTo(Variable other)
        {
            if (this.Name == other.Name && this.Type == other.Type && this.Domain == other.Domain)
            {
                return this.QuestionText == other.QuestionText;
            }

            return false;
        }

        public override Variable MakeCopy(bool linked = true)
        {
            return new Variable(this, linked);
        }

        public override bool IsValid()
        {
            if (this.ExpertSystemShell != null && this.Domain != null)
            {
                return this.Name.Length > 0;
            }

            return false;
        }

        protected override void InnerReplaceOriginal()
        {
            this.original.Name = this.Name;
            this.original.Domain = this.Domain;
            this.original.Type = this.Type;
            this.original.QuestionText = this.QuestionText;
        }

        public override bool IsAtDefaultState()
        {
            return this.IsEqualTo(new Variable(this.ExpertSystemShell));
        }

        private string GenerateQuestionFromName()
        {
            return $"{this.Name} = ?";
        }
    }
}
