﻿using System;

namespace ExpertSystem.Model
{
    public class VariableDeducedEventArgs : EventArgs
    {
        public DomainValue Value { get; set; }

        public bool HasError { get; set; }

        public string Error { get; set; }
    }
}
