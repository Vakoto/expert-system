﻿using System;

namespace ExpertSystem.Model
{
    public class VariableRequestEventArgs : EventArgs
    {
        public Variable Variable { get; set; }

        public DomainValue Value { get; set; }
    }
}
