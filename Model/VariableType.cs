﻿using System.ComponentModel;

namespace ExpertSystem.Model
{
    public enum VariableType
    {
        [Description("Запрашиваемая")]
        Requested,

        [Description("Выводимая")]
        Deducible,

        [Description("Запрашиваемо-выводимая")]
        RequestedDeducible,
    }
}
