﻿using System.Collections.Generic;

namespace ExpertSystem.Model
{
    public class WorkingMemory
    {
        public Variable GoalVariable { get; set; }

        public List<Fact> TrueFacts { get; set; }

        public List<Rule> UntriggeredRules { get; set; }

        public List<Rule> TriggeredRules { get; }

        public Stack<Rule> RulesStack { get; }

        public Dictionary<Variable, Rule> InferenceDictionary { get; }

        public WorkingMemory()
        {
            this.TrueFacts = new List<Fact>();
            this.UntriggeredRules = new List<Rule>();
            this.TriggeredRules = new List<Rule>();
            this.RulesStack = new Stack<Rule>();
            this.InferenceDictionary = new Dictionary<Variable, Rule>();
        }
    }
}
