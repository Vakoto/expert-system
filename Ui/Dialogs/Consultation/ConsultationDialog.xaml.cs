﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ExpertSystem.Model;

namespace ExpertSystem.Ui.Dialogs.Consultation
{
    public partial class ConsultationDialog
    {
        private ConsultationState State;

        private readonly ManualResetEvent valueGetEvent;

        private DomainValue currentValue;

        private ExpertSystemShell ExpertSystemShell { get; set; }

        private InferenceEngine InferenceEngine
        {
            get => this.ExpertSystemShell.InferenceEngine;
            set => this.ExpertSystemShell.InferenceEngine = value;
        }

        public ConsultationDialog(ExpertSystemShell expertSystemShell)
        {
            this.ExpertSystemShell = expertSystemShell;
            this.valueGetEvent = new ManualResetEvent(false);
            this.InitializeComponent();
            if (this.ExpertSystemShell.Variables.Any(v => v.Type == VariableType.Deducible))
            {
                this.StartNewConsultation();
            }
            else
            {
                this.AddMessage(new ConsultationMessage(MessageType.Error,
                    "Невозможно начать консультацию:\nЭС не содержит выводимых переменных"));
                this.SetUiEnabled(false);
            }
        }

        private void StartNewConsultation()
        {
            this.InitializeLim();
            this.State = ConsultationState.ChoosingGoal;
            //this.spMessages.Children.Clear();
            this.tbDialog.Text = string.Empty;
            this.AddMessage(new ConsultationMessage(MessageType.Question, "Выберите цель консультации"));
            this.cbAnswers.ItemsSource = this.ExpertSystemShell
                .Variables
                .Where(v => v.Type == VariableType.Deducible);

            if (this.ExpertSystemShell.GoalVariable != null)
            {
                this.cbAnswers.SelectedItem = this.ExpertSystemShell.GoalVariable;
            }
            else if (this.cbAnswers.Items.Count > 0)
            {
                this.cbAnswers.SelectedIndex = 0;
            }

            this.SetUiEnabled(true);
            this.grCombo.Visibility = Visibility.Visible;
            this.grButtons.Visibility = Visibility.Collapsed;
        }

        private void InitializeLim()
        {
            if (this.InferenceEngine != null)
            {
                this.InferenceEngine.VariableRequested -= this.Lim_VariableRequested;
                this.InferenceEngine.VariableDeduced -= this.Lim_VariableDeduced;
            }

            this.InferenceEngine = new InferenceEngine(this.ExpertSystemShell);
            this.InferenceEngine.VariableRequested += this.Lim_VariableRequested;
            this.InferenceEngine.VariableDeduced += this.Lim_VariableDeduced;
        }

        private void Lim_VariableDeduced(object sender, VariableDeducedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                if (e.HasError)
                {
                    this.AddMessage(new ConsultationMessage(MessageType.Error, "Ошибка вывода:\n" + e.Error));
                }
                else if (e.Value == null)
                {
                    this.AddMessage(new ConsultationMessage(MessageType.Error,
                        "Цель консультации не была достигнута.\nОбратитесь к другой ЭС."));
                }
                else
                {
                    this.AddMessage(new ConsultationMessage(MessageType.Result,
                        "Цель консультации достигнута!\nРезультат:" + string.Format("\n{0} - {1}",
                            this.ExpertSystemShell.GoalVariable.Name, e.Value.Name)));
                }

                this.grCombo.Visibility = Visibility.Collapsed;
                this.grButtons.Visibility = Visibility.Visible;
            }));
        }

        private void SetUiEnabled(bool enabled)
        {
            this.cbAnswers.IsEnabled = enabled;
            this.btAnswer.IsEnabled = enabled;
        }

        private void Lim_VariableRequested(object sender, VariableRequestEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                this.State = ConsultationState.RequestingVariable;
                this.cbAnswers.ItemsSource = e.Variable.Domain.Values;
                if (this.cbAnswers.Items.Count > 0)
                {
                    this.cbAnswers.SelectedIndex = 0;
                }

                this.AddMessage(new ConsultationMessage(MessageType.Question, e.Variable.QuestionText));
                this.SetUiEnabled(true);
            }));
            this.valueGetEvent.WaitOne();
            this.valueGetEvent.Reset();
            e.Value = this.currentValue;
        }

        private void AddMessage(ConsultationMessage msg)
        {
            // this.spMessages.Children.Add(msg);
            // this.svMessages.ScrollToEnd();
            this.tbDialog.Text = msg.Text;
        }

        private void btAnswer_Click(object sender, RoutedEventArgs e)
        {
            this.OnAnswer();
        }

        private void OnAnswer()
        {
            this.AddMessage(new ConsultationMessage(MessageType.Answer, this.cbAnswers.Text));
            if (this.State == ConsultationState.ChoosingGoal)
            {
                this.ExpertSystemShell.GoalVariable = this.cbAnswers.SelectedItem as Variable;
                Task.Run(() => this.InferenceEngine.DeduceGoalVariable(this.ExpertSystemShell.GoalVariable));
            }
            else if (this.State == ConsultationState.RequestingVariable)
            {
                this.currentValue = this.cbAnswers.SelectedItem as DomainValue;
                this.valueGetEvent.Set();
            }

            this.cbAnswers.ItemsSource = null;
            this.SetUiEnabled(false);
        }

        private void btShowExplanation_Click(object sender, RoutedEventArgs e)
        {
            var explanationWindow = new ExplanationWindow(this.InferenceEngine.workingMemory) { Owner = this };
            explanationWindow.ShowDialog();
        }

        private void btNewConsult_Click(object sender, RoutedEventArgs e)
        {
            this.StartNewConsultation();
        }

        private enum ConsultationState
        {
            ChoosingGoal,
            RequestingVariable,
        }
    }
}
