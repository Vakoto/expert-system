﻿using System.Windows;
using System.Windows.Media;

namespace ExpertSystem.Ui.Dialogs.Consultation
{
    public partial class ConsultationMessage
    {
        private MessageType msgType;

        public string Text { get; }

        public MessageType MsgType
        {
            get => this.msgType;
            private set
            {
                switch (value)
                {
                    case MessageType.Answer:
                        this.brdMessage.Background = this.ColorFromHex("#d0dfef");
                        this.HorizontalAlignment = HorizontalAlignment.Right;
                        break;
                    case MessageType.Question:
                        this.brdMessage.Background = this.ColorFromHex("#ffffff");
                        this.HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                    case MessageType.Error:
                        this.brdMessage.Background = this.ColorFromHex("#efd0d0");
                        this.HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                    case MessageType.Result:
                        this.brdMessage.Background = this.ColorFromHex("#d3efd0");
                        this.HorizontalAlignment = HorizontalAlignment.Left;
                        break;
                }

                this.msgType = value;
            }
        }

        public ConsultationMessage(MessageType msgType, string text)
        {
            this.InitializeComponent();
            this.MsgType = msgType;
            this.tbMessage.Text = text;
            Text = text;
        }

        private SolidColorBrush ColorFromHex(string hex)
        {
            return (SolidColorBrush) new BrushConverter().ConvertFrom(hex);
        }
    }
}
