﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ExpertSystem.Model;

namespace ExpertSystem.Ui.Dialogs.Consultation
{
    public partial class ExplanationWindow
    {
        private readonly WorkingMemory workingMemory;

        private bool isExpanded;

        public ExplanationWindow(WorkingMemory workingMemory)
        {
            this.InitializeComponent();
            this.workingMemory = workingMemory;
            this.isExpanded = false;
            this.lvVars.ItemsSource = workingMemory.TrueFacts;
            this.InitializeTreeView();
        }

        private void InitializeTreeView()
        {
            this.InitializeTreeViewItem(this.workingMemory.GoalVariable);
        }

        private void InitializeTreeViewItem(Variable variable, TreeViewItem parent = null)
        {
            var str1 = "ЦЕЛЬ: " + variable.Name;
            var fact1 = this.workingMemory
                .TrueFacts
                .FirstOrDefault(f => f.Variable == variable);

            var str2 = fact1 == null ? str1 + " НЕ УДАЛОСЬ ВЫВЕСТИ" : str1 + " = " + fact1.DomainValue.Name;
            if (variable.Type == VariableType.Requested)
            {
                str2 += " [ЗАПРОШЕНА]";
            }

            var treeViewItem1 = new TreeViewItem { Header = str2 };
            var parent1 = treeViewItem1;
            if (parent != null)
            {
                parent.Items.Add(parent1);
            }
            else
            {
                this.tvRules.Items.Add(parent1);
            }

            try
            {
                var inference = this.workingMemory.InferenceDictionary[variable];
                var items = parent1.Items;
                var treeViewItem2 = new TreeViewItem { Header = inference.ToString };
                items.Add(treeViewItem2);
                foreach (var fact2 in inference.Premise)
                {
                    this.InitializeTreeViewItem(fact2.Variable, parent1);
                }
            }
            catch (KeyNotFoundException)
            {
            }
        }

        private void expCollapseLink_Click(object sender, RoutedEventArgs e)
        {
            this.expCollapseText.Content = this.isExpanded ? "Раскрыть" : "Скрыть";
            this.SetTreeViewItems(this.tvRules, !this.isExpanded);
            this.isExpanded = !this.isExpanded;
        }

        private void SetTreeViewItems(object obj, bool expand)
        {
            if (obj is TreeViewItem item)
            {
                item.IsExpanded = expand;
                foreach (var obj1 in item.Items)
                {
                    this.SetTreeViewItems(obj1, expand);
                }
            }
            else
            {
                if (!(obj is ItemsControl))
                {
                    return;
                }

                foreach (var obj1 in ((ItemsControl)obj).Items)
                {
                    if (obj1 != null)
                    {
                        this.SetTreeViewItems(((ItemsControl)obj).ItemContainerGenerator.ContainerFromItem(obj1), expand);
                        if (obj1 is TreeViewItem treeViewItem)
                        {
                            treeViewItem.IsExpanded = expand;
                        }
                    }
                }
            }
        }
    }
}
