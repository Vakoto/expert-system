﻿namespace ExpertSystem.Ui.Dialogs.Consultation
{
    public enum MessageType
    {
        Answer,
        Question,
        Error,
        Result,
    }
}
