﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using ExpertSystem.Model;
using ExpertSystem.Model.Exceptions;
using ExpertSystem.WpfTools.ServiceProviders.UI;

namespace ExpertSystem.Ui.Dialogs
{
    public partial class DomainDialog
    {
        private readonly DispatcherTimer nameChangedTimer;

        private ListViewDragDropManager<DomainValue> valuesDragManager;

        public Domain Domain { get; set; }

        public DomainDialog(ExpertSystemShell expertSystem, Domain domain = null)
        {
            this.Domain = domain?.MakeCopy() ?? new Domain(expertSystem);
            this.InitializeComponent();
            this._this.Title = domain == null ? "Создание домена" : "Изменение домена";
            this.lbDomainValues.ItemsSource = this.Domain.Values;
            this.nameChangedTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromSeconds(1.0)
            };
            this.nameChangedTimer.Tick += (sender, args) => this.OnNameChanged();
            this.valuesDragManager = new ListViewDragDropManager<DomainValue>(this.lbDomainValues);
            this.tbDomainName.Focus();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!this.lbNameError.IsVisible && this.Domain.IsValid())
            {
                if (this.Domain.IsCopy())
                {
                    var needReplaceOriginal = true;
                    if (this.Domain.HasValuesChanges() &&
                        this.Domain.ExpertSystemShell.Variables.Any(v => v.Domain == this.Domain.Original))
                    {
                        if (this.Domain.Original.Values.Any(value =>
                        {
                            if (this.Domain.Values.All(v => v.Original != value))
                            {
                                return this.Domain.ExpertSystemShell.Rules.Any(r =>
                                {
                                    if (r.Premise.All(p => p.DomainValue != value))
                                    {
                                        return r.Conclusion.Any(c => c.DomainValue == value);
                                    }

                                    return true;
                                });
                            }

                            return false;
                        }))
                        {
                            if (MessageBox.Show(
                                    "Внести изменения в текущий домен невозможно: было удалено значение, которое используется по крайней мере в одном правиле. Сохранить изменения как копию текущего домена?",
                                    "Подтверждение", MessageBoxButton.OKCancel) != MessageBoxResult.OK)
                            {
                                return;
                            }

                            needReplaceOriginal = false;
                        }
                        else
                        {
                            switch (MessageBox.Show(
                                "Текущий домен используется как минимум в одной переменной. Сохранить изменения как копию текущего домена?",
                                "Подтверждение", MessageBoxButton.YesNoCancel))
                            {
                                case MessageBoxResult.Cancel:
                                    return;
                                case MessageBoxResult.Yes:
                                    needReplaceOriginal = false;
                                    break;
                            }
                        }
                    }

                    if (needReplaceOriginal)
                    {
                        this.Domain.ReplaceOriginal();
                    }
                    else
                    {
                        this.Domain.MakeOriginal();
                        var newName = this.Domain.Name + " Копия";
                        var num = 1;
                        while (this.Domain
                            .ExpertSystemShell
                            .Domains
                            .Any(d => d.Name.Equals(newName, StringComparison.OrdinalIgnoreCase)))
                        {
                            newName += num.ToString();
                            ++num;
                        }

                        this.Domain.Name = newName;
                        this.Domain.ExpertSystemShell.Domains.Add(this.Domain);
                    }
                }
                else
                {
                    this.Domain.ExpertSystemShell.Domains.Add(this.Domain);
                }

                this.DialogResult = true;
            }
            else
            {
                var stringList = new List<string>();
                if (this.lbNameError.IsVisible)
                {
                    stringList.Add(this.lbNameError.Content + ".");
                }

                if (this.Domain.Name.Length == 0)
                {
                    stringList.Add("Пустое имя домена");
                }

                if (this.Domain.Values.Count == 0)
                {
                    stringList.Add("Пустое множество значений домена");
                }


                var num = (int) MessageBox.Show(string.Join(Environment.NewLine, stringList),
                    "Ошибка", MessageBoxButton.OK);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (DialogResult is true || !this.Domain.HasChanges() ||
                MessageBox.Show("Вы уверены, что хотите выйти без сохранения?",
                    "Подтверждение", MessageBoxButton.YesNo) != MessageBoxResult.No)
            {
                return;
            }

            e.Cancel = true;
        }

        private void OnEmptyValue()
        {
            var num = (int) MessageBox.Show("Значение не может быть пустым");
        }

        private void OnDuplicateValue(string duplicate)
        {
            var num = (int) MessageBox.Show(string.Format("{0} уже есть в множестве значений", duplicate));
        }

        private void btnAddValue_Click(object sender, RoutedEventArgs e)
        {
            this.OnAddValue();
        }

        private void OnAddValue()
        {
            try
            {
                this.lbDomainValues.SelectedItem =
                    this.Domain.AddValue(this.tbDomainValueName.Text.Trim());
                this.tbDomainValueName.Focus();
                this.tbDomainValueName.SelectAll();
            }
            catch (EmptyValueException)
            {
                this.OnEmptyValue();
            }
            catch (DomainValueAlreadyExistsException ex)
            {
                this.OnDuplicateValue(ex.Value);
            }
        }

        private void btnDeleteValue_Click(object sender, RoutedEventArgs e)
        {
            this.OnRemoveValue();
        }

        private void OnRemoveValue()
        {
            var selectedIndex = this.lbDomainValues.SelectedIndex;
            this.Domain.Values.Remove(this.lbDomainValues.SelectedValue as DomainValue);
            if (selectedIndex < this.lbDomainValues.Items.Count - 1)
            {
                this.lbDomainValues.SelectedIndex = selectedIndex;
            }
            else
            {
                if (this.lbDomainValues.Items.Count == 0)
                {
                    return;
                }

                this.lbDomainValues.SelectedIndex = this.lbDomainValues.Items.Count - 1;
            }
        }

        private void btnEditValue_Click(object sender, RoutedEventArgs e)
        {
            this.OnEditValue();
        }

        private void OnEditValue()
        {
            try
            {
                var value = this.Domain.Values[this.lbDomainValues.SelectedIndex];
                value.Name = this.tbDomainValueName.Text.Trim();

                DomainValue domainValue = null;
                if (Domain.Original != null)
                {
                    domainValue = Domain.Original
                        .Values
                        .FirstOrDefault(x => x.IsEqualTo(value));
                }

                if (domainValue != null)
                {
                    var selectedIndex = this.lbDomainValues.SelectedIndex;
                    this.Domain.Values[selectedIndex] = domainValue.MakeCopy(true);
                    this.lbDomainValues.SelectedItem = this.Domain.Values[selectedIndex];
                }

                this.tbDomainValueName.Focus();
                this.tbDomainValueName.SelectAll();
            }
            catch (EmptyValueException)
            {
                this.OnEmptyValue();
            }
            catch (DomainValueAlreadyExistsException ex)
            {
                this.OnDuplicateValue(ex.Value);
            }
        }

        private void tbDomainName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.nameChangedTimer?.Start();
        }

        private void OnNameChanged()
        {
            try
            {
                this.Domain.Name = this.tbDomainName.Text.Trim();
                this.lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameAlreadyExistsException)
            {
                this.lbNameError.Visibility = Visibility.Visible;
            }
        }

        private void tbDomainValueName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Return)
            {
                return;
            }

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.OnEditValue();
            }
            else
            {
                this.OnAddValue();
            }

            e.Handled = true;
        }

        private void lbDomainValues_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var num = (int) MessageBox.Show("Неизвестная ошибка");
        }

        private void lbDomainValues_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Delete || this.lbDomainValues.SelectedItem == null)
            {
                return;
            }

            this.OnRemoveValue();
            e.Handled = true;
        }
    }
}
