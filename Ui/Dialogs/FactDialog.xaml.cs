﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ExpertSystem.Model;
using ExpertSystem.Model.Exceptions;

namespace ExpertSystem.Ui.Dialogs
{
    public partial class FactDialog
    {
        private bool isPremise;

        public Model.ExpertSystemShell ExpertSystem { get; set; }

        public Rule Rule { get; set; }

        public Fact Fact { get; set; }

        public FactDialog(ExpertSystemShell expertSystem, Rule rule, bool isPremise, Fact fact = null)
        {
            this.Fact = fact?.MakeCopy() ?? new Fact();
            this.isPremise = isPremise;
            this.ExpertSystem = expertSystem;
            this.Rule = rule;
            this.InitializeComponent();
            this.cbVariable.ItemsSource = isPremise
                ? this.ExpertSystem.Variables
                : this.ExpertSystem.Variables.Where(v => (uint) v.Type > 0U);

            if (this.Fact.Variable == null)
            {
                this.cbVariable.SelectedIndex = 0;
            }

            var str = isPremise ? "посылки" : "заключения";
            this.Title = fact == null ? "Добавление факта " + str : "Редактирование факта " + str;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (this.Fact.IsValid())
            {
                if (this.Fact.IsCopy())
                {
                    this.Fact.ReplaceOriginal();
                }
                else
                {
                    try
                    {
                        if (this.isPremise)
                        {
                            this.Rule.AddPremise(this.Fact);
                        }
                        else
                        {
                            this.Rule.AddConclusion(this.Fact);
                        }
                    }
                    catch (FactAlreadyExistsException ex)
                    {
                        var num = (int) MessageBox.Show(
                            string.Format("{0} уже содержит факт с переменной '{1}'.",
                                this.isPremise ? "Посылка" : "Заключение", ex.Name),
                            "Ошибка", MessageBoxButton.OK);
                        return;
                    }
                }

                this.DialogResult = true;
            }
            else
            {
                var stringList = new List<string>();
                if (this.Fact.Variable == null)
                {
                    stringList.Add("Необходимо задать переменную.");
                }

                if (this.Fact.DomainValue == null)
                {
                    stringList.Add("Необходимо задать значение переменной.");
                }

                var num = (int) MessageBox.Show(string.Join(Environment.NewLine, stringList),
                    "Ошибка", MessageBoxButton.OK);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var variableDialog1 = new VariableDialog(this.ExpertSystem)
            {
                Owner = this
            };
            var variableDialog2 = variableDialog1;
            if (!(variableDialog2.ShowDialog() is true))
            {
                return;
            }

            if (!this.isPremise)
            {
                this.cbVariable.ItemsSource = this.ExpertSystem.Variables.Where(v => (uint) v.Type > 0U);
            }

            this.cbVariable.SelectedItem = variableDialog2.Variable;
            this.cbValue.SelectedIndex = 0;
        }

        private void thisWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.DialogResult is true || !this.Fact.HasChanges() ||
                MessageBox.Show("В факт были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                    "Подтверждение", MessageBoxButton.YesNo) != MessageBoxResult.No)
            {
                return;
            }

            e.Cancel = true;
        }

        private void cbVariable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.Fact.DomainValue != null)
            {
                return;
            }

            this.cbValue.SelectedIndex = 0;
        }
    }
}
