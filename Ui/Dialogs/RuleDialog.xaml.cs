﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using ExpertSystem.Model;
using ExpertSystem.Model.Exceptions;
using ExpertSystem.WpfTools.ServiceProviders.UI;

namespace ExpertSystem.Ui.Dialogs
{
    public partial class RuleDialog
    {
        private readonly Rule selectedRule;

        private readonly DispatcherTimer nameChangedTimer;

        private readonly ListViewDragDropManager<Fact> premiseDragManager;

        private readonly ListViewDragDropManager<Fact> conclusionDragManager;

        public Rule Rule { get; set; }

        public RuleDialog(ExpertSystemShell expertSystem, Rule rule, bool makeNew)
        {
            this.Rule = makeNew ? new Rule(expertSystem) : rule.MakeCopy();
            this.selectedRule = rule;
            this.InitializeComponent();
            this.Title = rule == null ? "Создание правила" : "Редактирование правила";
            this.nameChangedTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(1.0)
            };
            this.nameChangedTimer.Tick += (sender, args) => this.OnNameChanged();
            this.premiseDragManager = new ListViewDragDropManager<Fact>(this.lvPremises);
            this.conclusionDragManager = new ListViewDragDropManager<Fact>(this.lvConclusions);
            this.premiseDragManager.ProcessDrop += this.PremiseDragManager_ProcessDrop;
            this.conclusionDragManager.ProcessDrop += this.ConclusionDragManager_ProcessDrop;
            this.tbRuleName.Focus();
        }

        private void ConclusionDragManager_ProcessDrop(object sender, ProcessDropEventArgs<Fact> e)
        {
            if (e.OldIndex <= -1)
            {
                return;
            }

            this.Rule.Conclusion.Move(e.OldIndex, e.NewIndex);
        }

        private void PremiseDragManager_ProcessDrop(object sender, ProcessDropEventArgs<Fact> e)
        {
            if (e.OldIndex <= -1)
            {
                return;
            }

            this.Rule.Premise.Move(e.OldIndex, e.NewIndex);
        }

        private void OnNameChanged()
        {
            try
            {
                this.Rule.Name = this.tbRuleName.Text.Trim();
                this.lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameAlreadyExistsException)
            {
                this.lbNameError.Visibility = Visibility.Visible;
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!this.lbNameError.IsVisible && this.Rule.IsValid())
            {
                if (this.Rule.IsCopy())
                {
                    this.Rule.ReplaceOriginal();
                }
                else if (this.selectedRule != null)
                {
                    this.Rule.ExpertSystemShell.Rules.Insert(
                        this.Rule.ExpertSystemShell.Rules.IndexOf(this.selectedRule) + 1, this.Rule);
                }
                else
                {
                    this.Rule.ExpertSystemShell.Rules.Add(this.Rule);
                }

                this.DialogResult = true;
            }
            else
            {
                var stringList = new List<string>();
                if (this.lbNameError.IsVisible)
                {
                    stringList.Add(this.lbNameError.Content + ".");
                }

                if (this.Rule.Name.Length == 0)
                {
                    stringList.Add("Имя правила не может быть пустым.");
                }

                if (this.Rule.Premise.Count == 0)
                {
                    stringList.Add("Посылка правила должна содержать по крайней мере один факт.");
                }

                if (this.Rule.Conclusion.Count == 0)
                {
                    stringList.Add("Заключение правила должно содержать по крайней мере один факт.");
                }

                var num = (int) MessageBox.Show(string.Join(Environment.NewLine, stringList),
                    "Ошибка", MessageBoxButton.OK);
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            var dialogResult = this.DialogResult;
            if ((dialogResult.GetValueOrDefault() ? (dialogResult.HasValue ? 1 : 0) : 0) != 0 ||
                !this.Rule.HasChanges() ||
                MessageBox.Show("В правило были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                    "Подтверждение", MessageBoxButton.YesNo) != MessageBoxResult.No)
            {
                return;
            }

            e.Cancel = true;
        }

        private void tbRuleName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.nameChangedTimer?.Start();
        }

        private void OnDuplicateFact(string duplicate, bool isPremise)
        {
            var num = (int) MessageBox.Show(string.Format("{0} уже содержит факт с переменной '{1}'",
                isPremise ? "Посылка" : "Заключение", duplicate));
        }

        private void btnAddPremise_Click(object sender, RoutedEventArgs e)
        {
            var factDialog = new FactDialog(this.Rule.ExpertSystemShell, this.Rule, true)
            {
                Owner = this
            };
            factDialog.ShowDialog();
        }

        private void btnEditPremise_Click(object sender, RoutedEventArgs e)
        {
            var factDialog = new FactDialog(this.Rule.ExpertSystemShell, this.Rule, true,
                this.lvPremises.SelectedItem as Fact)
            {
                Owner = this
            };
            factDialog.ShowDialog();
        }

        private void btnRemovePremise_Click(object sender, RoutedEventArgs e)
        {
            this.Rule.Premise.Remove(this.lvPremises.SelectedItem as Fact);
        }

        private void btnAddConcl_Click(object sender, RoutedEventArgs e)
        {
            var factDialog = new FactDialog(this.Rule.ExpertSystemShell, this.Rule, false)
            {
                Owner = this
            };
            factDialog.ShowDialog();
        }

        private void btnEditConcl_Click(object sender, RoutedEventArgs e)
        {
            var factDialog = new FactDialog(this.Rule.ExpertSystemShell, this.Rule, false,
                this.lvConclusions.SelectedItem as Fact)
            {
                Owner = this
            };
            factDialog.ShowDialog();
        }

        private void btnRemoveConcl_Click(object sender, RoutedEventArgs e)
        {
            this.Rule.Conclusion.Remove(this.lvConclusions.SelectedItem as Fact);
        }
    }
}
