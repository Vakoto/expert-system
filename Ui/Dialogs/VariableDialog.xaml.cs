﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using ExpertSystem.Model;
using ExpertSystem.Model.Exceptions;

namespace ExpertSystem.Ui.Dialogs
{
    public partial class VariableDialog
    {
        private readonly DispatcherTimer nameChangedTimer;

        public Variable Variable { get; set; }

        public VariableDialog(ExpertSystemShell expertSystemShell, Variable variable = null)
        {
            this.Variable = variable?.MakeCopy() ?? new Variable(expertSystemShell);
            this.InitializeComponent();
            this.thisWindow.Title = variable == null ? "Создание переменной" : "Редактирование переменной";
            this.cbDomains.ItemsSource = expertSystemShell.Domains;
            this.rbStack.Children.Clear();
            foreach (var variableType in (VariableType[]) Enum.GetValues(typeof(VariableType)))
            {
                var radioButton1 = new RadioButton
                {
                    Content = this.Variable.GetTypeDescription(variableType),
                    IsChecked = variableType == this.Variable.Type,
                    Tag = variableType
                };
                var radioButton2 = radioButton1;
                radioButton2.Checked += this.OnRadioTypeChanged;
                this.rbStack.Children.Add(radioButton2);
            }

            this.nameChangedTimer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(1.0)
            };
            this.nameChangedTimer.Tick += (sender, args) => this.OnNameChanged();
            this.Variable.PropertyChanged += this.Variable_PropertyChanged;
            this.AfterTypeChanged();
            this.tbVariableName.Focus();
        }

        private void OnRadioTypeChanged(object sender, RoutedEventArgs e)
        {
            this.Variable.Type = (VariableType) (sender as RadioButton).Tag;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (!this.lbNameError.IsVisible && this.Variable.IsValid())
            {
                if (this.Variable.Type == VariableType.Deducible)
                {
                    this.Variable.QuestionText = null;
                }

                if (this.Variable.IsCopy())
                {
                    var needReplaceOriginal = true;
                    if (this.Variable.Domain != this.Variable.Original.Domain &&
                        this.Variable.ExpertSystemShell.Rules.Any(r =>
                        {
                            if (r.Premise.All(p => p.Variable != this.Variable.Original))
                            {
                                return r.Conclusion.Any(c => c.Variable == this.Variable.Original);
                            }

                            return true;
                        }))
                    {
                        needReplaceOriginal = false;
                        if (MessageBox.Show(
                                "Изменение домена переменной, которая используется в правилах, невозможно. Сохранить изменения как копию текущей переменной?",
                                "Подтверждение", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                        {
                            return;
                        }
                    }

                    if (needReplaceOriginal)
                    {
                        this.Variable.ReplaceOriginal();
                    }
                    else
                    {
                        this.Variable.MakeOriginal();
                        var newName = this.Variable.Name + " Копия";
                        var num = 1;
                        while (this.Variable.ExpertSystemShell.Variables.Any(x => x.Name.Equals(newName, StringComparison.OrdinalIgnoreCase)))
                        {
                            newName += num.ToString();
                            ++num;
                        }

                        this.Variable.Name = newName;
                        this.Variable.ExpertSystemShell.Variables.Add(this.Variable);
                    }
                }
                else
                {
                    this.Variable.ExpertSystemShell.Variables.Add(this.Variable);
                }

                this.DialogResult = true;
            }
            else
            {
                var stringList = new List<string>();
                if (this.lbNameError.IsVisible)
                {
                    stringList.Add(this.lbNameError.Content + ".");
                }

                if (this.Variable.Name.Length == 0)
                {
                    stringList.Add("Имя переменной не может быть пустым.");
                }

                if (this.Variable.Domain == null)
                {
                    stringList.Add("Необходимо задать домен допустимых значений.");
                }

                var num = (int) MessageBox.Show(string.Join(Environment.NewLine, stringList),
                    "Ошибка", MessageBoxButton.OK);
            }
        }

        private void Variable_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "Type")
            {
                return;
            }

            this.AfterTypeChanged();
        }

        private void AfterTypeChanged()
        {
            if (this.Variable.Type == VariableType.Deducible)
            {
                this.tbQuestion.Background = Brushes.LightGray;
            }
            else
            {
                this.tbQuestion.Background = Brushes.White;
            }

            this.tbQuestion.IsEnabled = this.Variable.Type != VariableType.Deducible;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
        }

        private void thisWindow_Closing(object sender, CancelEventArgs e)
        {
            var dialogResult = this.DialogResult;
            if ((dialogResult.GetValueOrDefault()? (dialogResult.HasValue ? 1 : 0) : 0) != 0 ||
                !this.Variable.HasChanges() ||
                MessageBox.Show("В переменную были внесены изменения, вы уверены, что хотите выйти без сохранения?",
                    "Подтверждение", MessageBoxButton.YesNo) != MessageBoxResult.No)
            {
                return;
            }

            e.Cancel = true;
        }

        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            var domainDialog1 = new DomainDialog(this.Variable.ExpertSystemShell) { Owner = this };
            var domainDialog2 = domainDialog1;
            if (!(domainDialog2.ShowDialog() is true))
            {
                return;
            }

            this.cbDomains.SelectedValue = domainDialog2.Domain;
        }

        private void tbVariableName_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.nameChangedTimer?.Start();
        }

        private void OnNameChanged()
        {
            this.nameChangedTimer?.Stop();
            try
            {
                this.Variable.Name = this.tbVariableName.Text.Trim();
                this.lbNameError.Visibility = Visibility.Hidden;
            }
            catch (NameAlreadyExistsException)
            {
                this.lbNameError.Visibility = Visibility.Visible;
            }
        }
    }
}
