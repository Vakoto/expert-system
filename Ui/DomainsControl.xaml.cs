﻿using System.Windows;
using System.Windows.Input;
using ExpertSystem.Model;
using ExpertSystem.Ui.Dialogs;
using MessageBox = System.Windows.MessageBox;

namespace ExpertSystem.Ui
{
    public partial class DomainsControl
    {
        private ExpertSystemShell expertSystem;

        public ExpertSystemShell ExpertSystem
        {
            get => this.expertSystem;
            set
            {
                this.expertSystem = value;
                this.lbDomains.ItemsSource = this.expertSystem.Domains;
            }
        }

        public DomainsControl()
        {
            this.InitializeComponent();
        }

        private void btnAddDomain_Click(object sender, RoutedEventArgs e)
        {
            var domainDialog = new DomainDialog(this.ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            domainDialog.ShowDialog();
        }

        private void btnEditDomain_Click(object sender, RoutedEventArgs e)
        {
            this.OnEditDomain();
        }

        private void OnEditDomain()
        {
            var domainDialog = new DomainDialog(this.ExpertSystem, this.lbDomains.SelectedItem as Domain)
            {
                Owner = Application.Current.MainWindow
            };
            domainDialog.ShowDialog();
        }

        private void btnRemoveDomain_Click(object sender, RoutedEventArgs e)
        {
            if (this.ExpertSystem.Remove(this.lbDomains.SelectedItem as Domain) || MessageBox.Show(
                    "Удаление домена невозможно пока он используется в существующих переменных.",
                    "Подтверждение", MessageBoxButton.OK) != MessageBoxResult.OK)
            {
                return;
            }

            //if (this.ExpertSystem.Remove(this.lbDomains.SelectedItem as Domain) || MessageBox.Show(
            //        "Удаление домена приведет к удалению всех переменных и правил, в которых он используется. Вы уверены, что хотите удалить домен?",
            //        "Подтверждение", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
            //{
            //    return;
            //}

            // this.ExpertSystem.Remove(this.lbDomains.SelectedItem as Domain, true);
        }

        private void lbDomains_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (((FrameworkElement) e.OriginalSource).DataContext == null)
            {
                return;
            }

            this.OnEditDomain();
        }
    }
}
