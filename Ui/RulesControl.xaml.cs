﻿using System.Windows;
using System.Windows.Input;
using ExpertSystem.Model;
using ExpertSystem.Ui.Dialogs;
using ExpertSystem.WpfTools.ServiceProviders.UI;

namespace ExpertSystem.Ui
{
    public partial class RulesControl
    {
        private ExpertSystemShell expertSystem;

        private ListViewDragDropManager<Rule> rulesDragManager;

        public ExpertSystemShell ExpertSystem
        {
            get => this.expertSystem;
            set
            {
                this.expertSystem = value;
                this.lvRules.ItemsSource = this.expertSystem.Rules;
            }
        }

        public RulesControl()
        {
            this.InitializeComponent();
            this.rulesDragManager = new ListViewDragDropManager<Rule>(this.lvRules);
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var ruleDialog1 = new RuleDialog(this.ExpertSystem, this.lvRules.SelectedItem as Rule, true)
            {
                Owner = Application.Current.MainWindow
            };
            var ruleDialog2 = ruleDialog1;
            ruleDialog2.ShowDialog();
            this.lvRules.SelectedItem = ruleDialog2.Rule;
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            this.OnEditRule();
        }

        private void OnEditRule()
        {
            var ruleDialog = new RuleDialog(this.ExpertSystem, this.lvRules.SelectedItem as Rule, false)
            {
                Owner = Application.Current.MainWindow
            };
            ruleDialog.ShowDialog();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            this.ExpertSystem.Remove(this.lvRules.SelectedItem as Rule);
        }

        private void lvRules_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (((FrameworkElement) e.OriginalSource).DataContext == null)
            {
                return;
            }

            this.OnEditRule();
        }
    }
}
