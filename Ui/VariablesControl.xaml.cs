﻿using System.Windows;
using System.Windows.Input;
using ExpertSystem.Model;
using ExpertSystem.Ui.Dialogs;
using MessageBox = System.Windows.MessageBox;

namespace ExpertSystem.Ui
{
    public partial class VariablesControl
    {
        private ExpertSystemShell expertSystem;

        public ExpertSystemShell ExpertSystem
        {
            get => this.expertSystem;
            set
            {
                this.expertSystem = value;
                this.lvVariables.ItemsSource = this.expertSystem.Variables;
            }
        }

        public VariablesControl()
        {
            this.InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var variableDialog = new VariableDialog(this.ExpertSystem)
            {
                Owner = Application.Current.MainWindow
            };
            variableDialog.ShowDialog();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            this.OnEditVariable();
        }

        private void OnEditVariable()
        {
            var variableDialog = new VariableDialog(this.ExpertSystem, this.lvVariables.SelectedItem as Variable)
            {
                Owner = Application.Current.MainWindow
            };
            variableDialog.ShowDialog();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (this.ExpertSystem.Remove(this.lvVariables.SelectedItem as Variable) || MessageBox.Show(
                    "Удаление переменной невозможно пока она используется в существующих правилах.",
                    "Подтверждение", MessageBoxButton.OK) == MessageBoxResult.OK)
            {
                return;
            }

            // this.ExpertSystem.Remove(this.lvVariables.SelectedItem as Domain, true);
        }

        private void lvVariables_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (((FrameworkElement) e.OriginalSource).DataContext == null)
            {
                return;
            }

            this.OnEditVariable();
        }
    }
}
