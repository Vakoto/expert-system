﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ExpertSystem.WpfTools.Adorners
{
    public class DragAdorner : Adorner
    {
        private readonly Rectangle child;

        private double offsetLeft;

        private double offsetTop;

        public DragAdorner(UIElement adornedElement, Size size, Brush brush)
            : base(adornedElement)
        {
            this.child = new Rectangle
            {
                Fill = brush,
                Width = size.Width,
                Height = size.Height,
                IsHitTestVisible = false
            };
        }

        public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            return new GeneralTransformGroup()
            {
                Children =
                {
                    base.GetDesiredTransform(transform),
                    new TranslateTransform(this.offsetLeft, this.offsetTop)
                }
            };
        }

        public double OffsetLeft
        {
            get => this.offsetLeft;
            set
            {
                this.offsetLeft = value;
                this.UpdateLocation();
            }
        }

        public void SetOffsets(double left, double top)
        {
            this.offsetLeft = left;
            this.offsetTop = top;
            this.UpdateLocation();
        }

        public double OffsetTop
        {
            get => this.offsetTop;
            set
            {
                this.offsetTop = value;
                this.UpdateLocation();
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
            this.child.Measure(constraint);
            return this.child.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            this.child.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
            return this.child;
        }

        protected override int VisualChildrenCount => 1;

        private void UpdateLocation()
        {
            (this.Parent as AdornerLayer)?.Update(this.AdornedElement);
        }
    }
}
