﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;

namespace ExpertSystem.WpfTools.Controls.Utilities
{
    public class MouseUtilities
    {
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(ref Win32Point pt);

        [DllImport("user32.dll")]
        private static extern bool ScreenToClient(IntPtr window, ref Win32Point pt);

        public static Point GetMousePosition(Visual relativeTo)
        {
            var point = new Win32Point();
            GetCursorPos(ref point);
            return relativeTo.PointFromScreen(new Point(point.X, point.Y));
        }

        private struct Win32Point
        {
            public int X;
            public int Y;
        }
    }
}
