﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using ExpertSystem.WpfTools.Adorners;
using ExpertSystem.WpfTools.Controls.Utilities;

namespace ExpertSystem.WpfTools.ServiceProviders.UI
{
    public class ListViewDragDropManager<TItemType> where TItemType : class
    {
        private bool canInitiateDrag;

        private DragAdorner dragAdorner;

        private double dragAdornerOpacity;

        private int indexToSelect;

        private TItemType itemUnderDragCursor;

        private ListView listView;

        private Point ptMouseDown;

        private bool showDragAdorner;

        public ListViewDragDropManager()
        {
            this.canInitiateDrag = false;
            this.dragAdornerOpacity = 0.7;
            this.indexToSelect = -1;
            this.showDragAdorner = true;
        }

        public ListViewDragDropManager(ListView listView)
            : this()
        {
            this.ListView = listView;
        }

        public ListViewDragDropManager(ListView listView, double dragAdornerOpacity)
            : this(listView)
        {
            this.DragAdornerOpacity = dragAdornerOpacity;
        }

        public ListViewDragDropManager(ListView listView, bool showDragAdorner)
            : this(listView)
        {
            this.ShowDragAdorner = showDragAdorner;
        }

        public double DragAdornerOpacity
        {
            get => this.dragAdornerOpacity;
            set
            {
                if (this.IsDragInProgress)
                {
                    throw new InvalidOperationException(
                        "Cannot set the DragAdornerOpacity property during a drag operation.");
                }

                if (value < 0.0 || value > 1.0)
                {
                    throw new ArgumentOutOfRangeException(nameof(DragAdornerOpacity), value,
                        "Must be between 0 and 1.");
                }

                this.dragAdornerOpacity = value;
            }
        }

        public bool IsDragInProgress { get; private set; }

        public ListView ListView
        {
            get => this.listView;
            set
            {
                if (this.IsDragInProgress)
                {
                    throw new InvalidOperationException("Cannot set the ListView property during a drag operation.");
                }

                if (this.listView != null)
                {
                    this.listView.PreviewMouseLeftButtonDown -= this.ListViewPreviewMouseLeftButtonDown;
                    this.listView.PreviewMouseMove -= this.ListViewPreviewMouseMove;
                    this.listView.DragOver -= this.ListViewDragOver;
                    this.listView.DragLeave -= this.ListViewDragLeave;
                    this.listView.DragEnter -= this.ListViewDragEnter;
                    this.listView.Drop -= this.ListViewDrop;
                }

                this.listView = value;
                if (this.listView == null)
                {
                    return;
                }

                if (!this.listView.AllowDrop)
                {
                    this.listView.AllowDrop = true;
                }

                this.listView.PreviewMouseLeftButtonDown += this.ListViewPreviewMouseLeftButtonDown;
                this.listView.PreviewMouseMove += this.ListViewPreviewMouseMove;
                this.listView.DragOver += this.ListViewDragOver;
                this.listView.DragLeave += this.ListViewDragLeave;
                this.listView.DragEnter += this.ListViewDragEnter;
                this.listView.Drop += this.ListViewDrop;
            }
        }

        public event EventHandler<ProcessDropEventArgs<TItemType>> ProcessDrop;

        public bool ShowDragAdorner
        {
            get => this.showDragAdorner;
            set
            {
                if (this.IsDragInProgress)
                {
                    throw new InvalidOperationException(
                        "Cannot set the ShowDragAdorner property during a drag operation.");
                }

                this.showDragAdorner = value;
            }
        }

        private void ListViewPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.IsMouseOverScrollbar)
            {
                this.canInitiateDrag = false;
            }
            else
            {
                var indexUnderDragCursor = this.IndexUnderDragCursor;
                this.canInitiateDrag = indexUnderDragCursor > -1;
                if (this.canInitiateDrag)
                {
                    this.ptMouseDown = MouseUtilities.GetMousePosition(this.listView);
                    this.indexToSelect = indexUnderDragCursor;
                }
                else
                {
                    this.ptMouseDown = new Point(-10000.0, -10000.0);
                    this.indexToSelect = -1;
                }
            }
        }

        private void ListViewPreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!this.CanStartDragOperation)
            {
                return;
            }

            if (this.listView.SelectedIndex != this.indexToSelect)
            {
                this.listView.SelectedIndex = this.indexToSelect;
            }

            if (this.listView.SelectedItem == null)
            {
                return;
            }

            var listViewItem = this.GetListViewItem(this.listView.SelectedIndex);
            if (listViewItem == null)
            {
                return;
            }

            var adornerLayer = this.ShowDragAdornerResolved ? this.InitializeAdornerLayer(listViewItem) : null;
            this.InitializeDragOperation(listViewItem);
            this.PerformDragOperation();
            this.FinishDragOperation(listViewItem, adornerLayer);
        }

        private void ListViewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Move;
            if (this.ShowDragAdornerResolved)
            {
                this.UpdateDragAdornerLocation();
            }

            var indexUnderDragCursor = this.IndexUnderDragCursor;
            this.ItemUnderDragCursor = indexUnderDragCursor < 0
                ? default(TItemType)
                : this.ListView.Items[indexUnderDragCursor] as TItemType;
        }

        private void ListViewDragLeave(object sender, DragEventArgs e)
        {
            if (this.IsMouseOver(this.listView))
            {
                return;
            }

            if (this.ItemUnderDragCursor != null)
            {
                this.ItemUnderDragCursor = default(TItemType);
            }

            if (this.dragAdorner == null)
            {
                return;
            }

            this.dragAdorner.Visibility = Visibility.Collapsed;
        }

        private void ListViewDragEnter(object sender, DragEventArgs e)
        {
            if (this.dragAdorner == null || this.dragAdorner.Visibility == Visibility.Visible)
            {
                return;
            }

            this.UpdateDragAdornerLocation();
            this.dragAdorner.Visibility = Visibility.Visible;
        }

        private void ListViewDrop(object sender, DragEventArgs e)
        {
            if (this.ItemUnderDragCursor != null)
            {
                this.ItemUnderDragCursor = default(TItemType);
            }

            e.Effects = DragDropEffects.None;
            if (!e.Data.GetDataPresent(typeof(TItemType)))
            {
                return;
            }

            var data = e.Data.GetData(typeof(TItemType)) as TItemType;
            if (data == null)
            {
                return;
            }

            var itemsSource = this.listView.ItemsSource as ObservableCollection<TItemType>;
            if (itemsSource == null)
            {
                throw new Exception(
                    "A ListView managed by ListViewDragManager must have its ItemsSource set to an ObservableCollection<ItemType>.");
            }

            var oldIndex = itemsSource.IndexOf(data);
            var num = this.IndexUnderDragCursor;
            if (num < 0)
            {
                if (itemsSource.Count == 0)
                {
                    num = 0;
                }
                else
                {
                    if (oldIndex >= 0)
                        return;
                    num = itemsSource.Count;
                }
            }

            if (oldIndex == num)
            {
                return;
            }

            if (this.ProcessDrop != null)
            {
                var e1 = new ProcessDropEventArgs<TItemType>(itemsSource, data, oldIndex, num, e.AllowedEffects);
                this.ProcessDrop(this, e1);
                e.Effects = e1.Effects;
            }
            else
            {
                if (oldIndex > -1)
                {
                    itemsSource.Move(oldIndex, num);
                }
                else
                {
                    itemsSource.Insert(num, data);
                }

                e.Effects = DragDropEffects.Move;
            }
        }

        private bool CanStartDragOperation
        {
            get
            {
                return Mouse.LeftButton == MouseButtonState.Pressed && this.canInitiateDrag &&
                       (this.indexToSelect != -1 && this.HasCursorLeftDragThreshold);
            }
        }

        private void FinishDragOperation(ListViewItem draggedItem, AdornerLayer adornerLayer)
        {
            ListViewItemDragState.SetIsBeingDragged(draggedItem, false);
            this.IsDragInProgress = false;
            if (this.ItemUnderDragCursor != null)
            {
                this.ItemUnderDragCursor = default(TItemType);
            }

            if (adornerLayer == null)
            {
                return;
            }

            adornerLayer.Remove(this.dragAdorner);
            this.dragAdorner = null;
        }

        private ListViewItem GetListViewItem(int index)
        {
            if (this.listView.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
            {
                return null;
            }

            return this.listView.ItemContainerGenerator.ContainerFromIndex(index) as ListViewItem;
        }

        private ListViewItem GetListViewItem(TItemType dataItem)
        {
            if (this.listView.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
            {
                return null;
            }

            return this.listView.ItemContainerGenerator.ContainerFromItem(dataItem) as ListViewItem;
        }

        private bool HasCursorLeftDragThreshold
        {
            get
            {
                if (this.indexToSelect < 0)
                {
                    return false;
                }

                var listViewItem = this.GetListViewItem(this.indexToSelect);
                var descendantBounds = VisualTreeHelper.GetDescendantBounds(listViewItem);
                var point = this.listView.TranslatePoint(this.ptMouseDown, listViewItem);
                var size = new Size(SystemParameters.MinimumHorizontalDragDistance * 2.0,
                    Math.Min(SystemParameters.MinimumVerticalDragDistance,
                        Math.Min(Math.Abs(point.Y), Math.Abs(descendantBounds.Height - point.Y))) * 2.0);
                var rect = new Rect(this.ptMouseDown, size);
                rect.Offset(size.Width / -2.0, size.Height / -2.0);
                var mousePosition = MouseUtilities.GetMousePosition(this.listView);
                return !rect.Contains(mousePosition);
            }
        }

        private int IndexUnderDragCursor
        {
            get
            {
                var num = -1;
                for (var index = 0; index < this.listView.Items.Count; ++index)
                {
                    if (this.IsMouseOver(this.GetListViewItem(index)))
                    {
                        num = index;
                        break;
                    }
                }

                return num;
            }
        }

        private AdornerLayer InitializeAdornerLayer(ListViewItem itemToDrag)
        {
            var visualBrush = new VisualBrush(itemToDrag);
            this.dragAdorner = new DragAdorner(this.listView, itemToDrag.RenderSize, visualBrush)
            {
                Opacity = this.DragAdornerOpacity
            };
            var adornerLayer = AdornerLayer.GetAdornerLayer(this.listView);
            adornerLayer.Add(this.dragAdorner);
            this.ptMouseDown = MouseUtilities.GetMousePosition(this.listView);
            return adornerLayer;
        }

        private void InitializeDragOperation(ListViewItem itemToDrag)
        {
            this.IsDragInProgress = true;
            this.canInitiateDrag = false;
            ListViewItemDragState.SetIsBeingDragged(itemToDrag, true);
        }

        private bool IsMouseOver(Visual target)
        {
            if (target == null)
            {
                return false;
            }

            return VisualTreeHelper.GetDescendantBounds(target).Contains(MouseUtilities.GetMousePosition(target));
        }

        private bool IsMouseOverScrollbar
        {
            get
            {
                var hitTestResult = VisualTreeHelper.HitTest(this.listView, MouseUtilities.GetMousePosition(this.listView));
                if (hitTestResult == null)
                {
                    return false;
                }

                for (var dependencyObject = hitTestResult.VisualHit;
                    dependencyObject != null;
                    dependencyObject = dependencyObject is Visual || dependencyObject is Visual3D
                        ? VisualTreeHelper.GetParent(dependencyObject)
                        : LogicalTreeHelper.GetParent(dependencyObject))
                {
                    if (dependencyObject is ScrollBar)
                        return true;
                }

                return false;
            }
        }

        private TItemType ItemUnderDragCursor
        {
            get => this.itemUnderDragCursor;
            set
            {
                if (this.itemUnderDragCursor == value)
                {
                    return;
                }

                for (var index = 0; index < 2; ++index)
                {
                    if (index == 1)
                    {
                        this.itemUnderDragCursor = value;
                    }

                    if (this.itemUnderDragCursor != null)
                    {
                        var listViewItem = this.GetListViewItem(this.itemUnderDragCursor);
                        if (listViewItem != null)
                        {
                            ListViewItemDragState.SetIsUnderDragCursor(listViewItem, index == 1);
                        }
                    }
                }
            }
        }

        private void PerformDragOperation()
        {
            var selectedItem = this.listView.SelectedItem as TItemType;
            var allowedEffects = DragDropEffects.Move | DragDropEffects.Link;
            if (DragDrop.DoDragDrop(this.listView, selectedItem, allowedEffects) == DragDropEffects.None)
            {
                return;
            }

            this.listView.SelectedItem = selectedItem;
        }

        private bool ShowDragAdornerResolved
        {
            get
            {
                if (this.ShowDragAdorner)
                {
                    return this.DragAdornerOpacity > 0.0;
                }

                return false;
            }
        }

        private void UpdateDragAdornerLocation()
        {
            if (this.dragAdorner == null)
            {
                return;
            }

            var mousePosition = MouseUtilities.GetMousePosition(this.ListView);
            this.dragAdorner.SetOffsets(mousePosition.X - this.ptMouseDown.X,
                this.GetListViewItem(this.indexToSelect).TranslatePoint(new Point(0.0, 0.0), this.ListView).Y +
                mousePosition.Y - this.ptMouseDown.Y);
        }
    }
}
