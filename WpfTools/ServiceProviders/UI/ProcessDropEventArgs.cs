﻿using System;
using System.Collections.ObjectModel;
using System.Windows;

namespace ExpertSystem.WpfTools.ServiceProviders.UI
{
    public class ProcessDropEventArgs<TItemType> : EventArgs where TItemType : class
    {
        public ObservableCollection<TItemType> ItemsSource { get; }

        public TItemType DataItem { get; }

        public int OldIndex { get; }

        public int NewIndex { get; }

        public DragDropEffects AllowedEffects { get; }

        public DragDropEffects Effects { get; set; }

        internal ProcessDropEventArgs(
            ObservableCollection<TItemType> itemsSource,
            TItemType dataItem,
            int oldIndex,
            int newIndex,
            DragDropEffects allowedEffects)
        {
            this.ItemsSource = itemsSource;
            this.DataItem = dataItem;
            this.OldIndex = oldIndex;
            this.NewIndex = newIndex;
            this.AllowedEffects = allowedEffects;
        }
    }
}
